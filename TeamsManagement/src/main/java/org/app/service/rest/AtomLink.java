package org.app.service.rest;

import java.io.Serializable;
import java.net.URI;
import java.net.URISyntaxException;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;

@XmlAccessorType(XmlAccessType.NONE)
public class AtomLink implements Serializable {
	private static final long serialVersionUID = 1L;

	private URI href;
	private String rel;
	private String type;

	public AtomLink() {
		super();
		// TODO Auto-generated constructor stub
	}

	public AtomLink(String href, String rel) throws URISyntaxException {
		super();
		this.href = new URI(href);
		this.rel = rel;
		this.type = "text/html";
	}

	@XmlAttribute(name = "href")
	public URI getHref() {
		return href;
	}

	public void setHref(URI href) {
		this.href = href;
	}

	@XmlAttribute(name = "rel")
	public String getRel() {
		return rel;
	}

	public void setRel(String rel) {
		this.rel = rel;
	}

	@XmlAttribute(name = "type")
	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}
	

}
