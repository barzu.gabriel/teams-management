package org.app.service.ejb;

import javax.ejb.Remote;

import org.app.patterns.EntityRepository;
import org.app.service.entities.Manager;

@Remote
public interface ManagerDataService extends EntityRepository<Manager> {

}