package org.app.service.ejb;

import java.util.Collection;
import java.util.logging.Logger;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.app.patterns.EntityRepositoryBase;
import org.app.service.entities.Employee;
import org.app.service.entities.Manager;

@Path("/managers")
@Stateless
@LocalBean
@SuppressWarnings("unused")
public class ManagerDataServiceEJB extends EntityRepositoryBase<Manager> implements ManagerDataService {
	private static Logger logger = Logger.getLogger(ManagerDataServiceEJB.class.getName());

	@PersistenceContext(unitName = "MSD")
	private EntityManager em;

	@Override
	public void setEm(EntityManager em) {
		// TODO Auto-generated method stub
		super.setEm(em);
	}

	@Override
	public Manager getById(Object id) {
		// TODO Auto-generated method stub
		return super.getById(id);
	}

	@Override
	public Collection<Manager> get(Manager entitySample) {
		// TODO Auto-generated method stub
		return super.get(entitySample);
	}

	@Override
	public Manager[] toArray() {
		// TODO Auto-generated method stub
		return super.toArray();
	}

	@Override
	public Manager add(Manager entity) {
		// TODO Auto-generated method stub
		return super.add(entity);
	}

	@Override
	public Collection<Manager> addAll(Collection<Manager> entities) {
		// TODO Auto-generated method stub
		return super.addAll(entities);
	}

	@Override
	public boolean remove(Manager entity) {
		// TODO Auto-generated method stub
		return super.remove(entity);
	}

	@Override
	public boolean removeAll(Collection<Manager> entities) {
		// TODO Auto-generated method stub
		return super.removeAll(entities);
	}

	@Override
	public int size() {
		// TODO Auto-generated method stub
		return super.size();
	}

	@Override
	public Manager refresh(Manager entity) {
		// TODO Auto-generated method stub
		return super.refresh(entity);
	}

	@Override
	public Class<Manager> getEntityParametrizedType() throws ClassCastException {
		// TODO Auto-generated method stub
		return super.getEntityParametrizedType();
	}

	@Override
	public int hashCode() {
		// TODO Auto-generated method stub
		return super.hashCode();
	}

	@Override
	public boolean equals(Object obj) {
		// TODO Auto-generated method stub
		return super.equals(obj);
	}

	@Override
	protected Object clone() throws CloneNotSupportedException {
		// TODO Auto-generated method stub
		return super.clone();
	}

	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return super.toString();
	}

	@Override
	protected void finalize() throws Throwable {
		// TODO Auto-generated method stub
		super.finalize();
	}

	@Override
	@GET
	@Produces({ MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON })
	public Collection<Manager> toCollection() {
		// TODO Auto-generated method stub
		logger.info("**** Debug to collection");
		return super.toCollection();
	}

	@GET
	@Path("/{id}")
	@Produces({ MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON })
	public Manager getById(@PathParam("id") Integer id) {
		// TODO Auto-generated method stub
		return super.getById(id);
	}

	@POST
	@Consumes({ MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON })
	@Produces({ MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON })
	public Collection<Manager> addIntoCollection(Manager manager) {
		super.add(manager);
		return super.toCollection();
	}

	@DELETE
	@Consumes({ MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON })
	@Produces({ MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON })
	public Collection<Manager> removeFromCollection(Manager manager) {
		super.remove(manager);
		return super.toCollection();
	}

	@DELETE
	@Path("/{id}")
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public void remove(@PathParam("id") Integer id) {
		Manager manager = super.getById(id);
		super.remove(manager);
	}

	@POST
	@Path("/new/{id}")
	@Produces({ MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON })
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public Manager createNewManager(@PathParam("id") Integer id) {
		logger.info(" *** Create new manager *** ");
		Manager manager = new Manager(id);
		this.add(manager);
		return manager;
	}
	
	@PUT
	@Path("/{id}")
	@Consumes({ MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON })
	@Produces({ MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON })
	public Collection<Manager> addIntoCollection(@PathParam("id") Integer managerId, Manager managerInput) {
		Manager manager = super.getById(managerId);
		manager.setContactPhone(managerInput.getContactPhone());
		this.add(manager);
		return super.toCollection();
	}
}
