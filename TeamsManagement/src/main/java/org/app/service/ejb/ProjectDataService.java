package org.app.service.ejb;

import javax.ejb.Remote;

import org.app.patterns.EntityRepository;
import org.app.service.entities.Project;

@Remote
public interface ProjectDataService extends EntityRepository<Project> {

}