package org.app.service.ejb;

import javax.ejb.Remote;

import org.app.patterns.EntityRepository;
import org.app.service.entities.Credential;

@Remote
public interface CredentialDataService extends EntityRepository<Credential> {

}