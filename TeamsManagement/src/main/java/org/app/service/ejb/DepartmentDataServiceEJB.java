package org.app.service.ejb;

import java.util.Collection;
import java.util.logging.Logger;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.app.patterns.EntityRepositoryBase;
import org.app.service.ejb.aggregation.DepartmentProjectDataServiceEJB;
import org.app.service.entities.Department;

@Path("/departments")
@Stateless
@LocalBean
@SuppressWarnings("unused")
public class DepartmentDataServiceEJB extends EntityRepositoryBase<Department> implements DepartmentDataService {
	private static Logger logger = Logger.getLogger(DepartmentDataServiceEJB.class.getName());

	@PersistenceContext(unitName = "MSD")
	private EntityManager em;

	@Override
	public void setEm(EntityManager em) {
		// TODO Auto-generated method stub
		super.setEm(em);
	}

	@Override
	public Collection<Department> get(Department entitySample) {
		// TODO Auto-generated method stub
		return super.get(entitySample);
	}

	@Override
	public Department[] toArray() {
		// TODO Auto-generated method stub
		return super.toArray();
	}

	@Override
	public Collection<Department> addAll(Collection<Department> entities) {
		// TODO Auto-generated method stub
		return super.addAll(entities);
	}

	@Override
	public boolean remove(Department entity) {
		// TODO Auto-generated method stub
		return super.remove(entity);
	}

	@Override
	public boolean removeAll(Collection<Department> entities) {
		// TODO Auto-generated method stub
		return super.removeAll(entities);
	}

	@Override
	public int size() {
		// TODO Auto-generated method stub
		return super.size();
	}

	@Override
	public Department refresh(Department entity) {
		// TODO Auto-generated method stub
		return super.refresh(entity);
	}

	@Override
	public Class<Department> getEntityParametrizedType() throws ClassCastException {
		// TODO Auto-generated method stub
		return super.getEntityParametrizedType();
	}

	@Override
	public int hashCode() {
		// TODO Auto-generated method stub
		return super.hashCode();
	}

	@Override
	public boolean equals(Object obj) {
		// TODO Auto-generated method stub
		return super.equals(obj);
	}

	@Override
	protected Object clone() throws CloneNotSupportedException {
		// TODO Auto-generated method stub
		return super.clone();
	}

	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return super.toString();
	}

	@Override
	protected void finalize() throws Throwable {
		// TODO Auto-generated method stub
		super.finalize();
	}

	@Override
	@GET
	@Produces({ MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON })
	public Collection<Department> toCollection() {
		// TODO Auto-generated method stub
		logger.info("**** Debug to collection");
		return super.toCollection();
	}

	@GET
	@Path("/{id}")
	@Produces({ MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON })
	public Department getById(@PathParam("id") Integer id) {
		// TODO Auto-generated method stub
		return super.getById(id);
	}

	@POST
	@Consumes({ MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON })
	@Produces({ MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON })
	public Collection<Department> addIntoCollection(Department department) {
		super.add(department);
		return super.toCollection();
	}

	@DELETE
	@Consumes({ MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON })
	@Produces({ MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON })
	public Collection<Department> removeFromCollection(Department department) {
		super.remove(department);
		return super.toCollection();
	}

	@DELETE
	@Path("/{id}")
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public void remove(@PathParam("id") Integer id) {
		Department department = super.getById(id);
		super.remove(department);
	}

	@POST
	@Path("/new/{id}")
	@Produces({ MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON })
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public Department createNewDepartment(@PathParam("id") Integer id) {
		logger.info(" *** Create new department *** ");
		Department department = new Department(id, "Department nr " + id + " with Postman");
		this.add(department);
		return department;
	}
	
	@PUT
	@Path("/{id}")
	@Consumes({ MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON })
	@Produces({ MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON })
	public Collection<Department> addIntoCollection(@PathParam("id") Integer id, Department departmentInput) {
		Department department = super.getById(id);
		department.setDepartmentName(departmentInput.getDepartmentName());
		this.add(department);
		return super.toCollection();
	}
}
