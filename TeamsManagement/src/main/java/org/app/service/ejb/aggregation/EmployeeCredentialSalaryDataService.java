package org.app.service.ejb.aggregation;

import javax.ejb.Remote;

import org.app.patterns.EntityRepository;
import org.app.service.entities.Employee;

@Remote
public interface EmployeeCredentialSalaryDataService extends EntityRepository<Employee> {

	Employee createNewEmployee(Integer employeeId, String firstName, String lastName, Double value);

}