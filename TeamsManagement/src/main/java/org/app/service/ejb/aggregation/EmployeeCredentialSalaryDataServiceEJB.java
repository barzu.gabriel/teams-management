package org.app.service.ejb.aggregation;

import java.util.Date;
import java.util.logging.Logger;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.app.patterns.EntityRepositoryBase;
import org.app.service.ejb.CredentialDataService;
import org.app.service.ejb.SalaryDataService;
import org.app.service.entities.Credential;
import org.app.service.entities.Employee;
import org.app.service.entities.Salary;

@Path("/employees")
@Stateless
@LocalBean
public class EmployeeCredentialSalaryDataServiceEJB extends EntityRepositoryBase<Employee>
		implements EmployeeCredentialSalaryDataService {
	private static Logger logger = Logger.getLogger(EmployeeCredentialSalaryDataServiceEJB.class.getName());

	@EJB
	private SalaryDataService salaryService;
	@EJB
	private CredentialDataService credentialService;

	@PostConstruct
	public void init() {
		logger.info("POSTCONSTRUCT-INIT: salaryService:     " + this.salaryService);
		logger.info("POSTCONSTRUCT-INIT: credentialService:     " + this.credentialService);
	}

	@POST
	@Path("/newEmployee/{id}/{firstName}/{lastName}/{value}")
	@Produces({ MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON })
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	@Override
	public Employee createNewEmployee(@PathParam("id") Integer employeeId, @PathParam("firstName") String firstName,
			@PathParam("lastName") String lastName, @PathParam("value") Double value) {

		Credential credEmp = new Credential(employeeId, null, firstName.toLowerCase() + "." + lastName.toLowerCase(),
				"parola");
		Salary slrEmp = new Salary(employeeId, null, value);
		Employee emp = new Employee(employeeId, firstName, lastName,
				firstName.toLowerCase() + "." + lastName.toLowerCase() + "@yahoo.com", new Date(), credEmp, slrEmp,
				null, null);
		credEmp.setPerson(emp);
		slrEmp.setPerson(emp);
		this.add(emp);
		return emp;
	}

}
