package org.app.service.ejb;

import javax.ejb.Remote;

import org.app.patterns.EntityRepository;
import org.app.service.entities.Absence;

@Remote
public interface AbsenceDataService extends EntityRepository<Absence> {

}