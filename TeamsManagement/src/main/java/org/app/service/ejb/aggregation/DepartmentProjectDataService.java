package org.app.service.ejb.aggregation;

import javax.ejb.Remote;

import org.app.patterns.EntityRepository;
import org.app.service.entities.Department;
import org.app.service.entities.Project;

@Remote
public interface DepartmentProjectDataService extends EntityRepository<Department> {

	Department createNewDepartment(Integer departmentId, Integer nrOfProjects);

	Project getProjectById(Integer projectId);

}