package org.app.service.ejb;

import java.util.Collection;
import java.util.logging.Logger;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.app.patterns.EntityRepositoryBase;
import org.app.service.entities.Absence;
import org.app.service.entities.Credential;
import org.app.service.entities.Salary;

@Path("/credentials")
@Stateless
@LocalBean
@SuppressWarnings("unused")
public class CredentialDataServiceEJB extends EntityRepositoryBase<Credential> implements CredentialDataService {
	private static Logger logger = Logger.getLogger(CredentialDataServiceEJB.class.getName());

	@PersistenceContext(unitName = "MSD")
	private EntityManager em;

	@Override
	public void setEm(EntityManager em) {
		// TODO Auto-generated method stub
		super.setEm(em);
	}

	@Override
	public Credential getById(Object id) {
		// TODO Auto-generated method stub
		return super.getById(id);
	}

	@Override
	public Collection<Credential> get(Credential entitySample) {
		// TODO Auto-generated method stub
		return super.get(entitySample);
	}

	@Override
	public Credential[] toArray() {
		// TODO Auto-generated method stub
		return super.toArray();
	}

	@Override
	public Credential add(Credential entity) {
		// TODO Auto-generated method stub
		return super.add(entity);
	}

	@Override
	public Collection<Credential> addAll(Collection<Credential> entities) {
		// TODO Auto-generated method stub
		return super.addAll(entities);
	}

	@Override
	public boolean remove(Credential entity) {
		// TODO Auto-generated method stub
		return super.remove(entity);
	}

	@Override
	public boolean removeAll(Collection<Credential> entities) {
		// TODO Auto-generated method stub
		return super.removeAll(entities);
	}

	@Override
	public int size() {
		// TODO Auto-generated method stub
		return super.size();
	}

	@Override
	public Credential refresh(Credential entity) {
		// TODO Auto-generated method stub
		return super.refresh(entity);
	}

	@Override
	public Class<Credential> getEntityParametrizedType() throws ClassCastException {
		// TODO Auto-generated method stub
		return super.getEntityParametrizedType();
	}

	@Override
	public int hashCode() {
		// TODO Auto-generated method stub
		return super.hashCode();
	}

	@Override
	public boolean equals(Object obj) {
		// TODO Auto-generated method stub
		return super.equals(obj);
	}

	@Override
	protected Object clone() throws CloneNotSupportedException {
		// TODO Auto-generated method stub
		return super.clone();
	}

	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return super.toString();
	}

	@Override
	protected void finalize() throws Throwable {
		// TODO Auto-generated method stub
		super.finalize();
	}

	@Override
	@GET
	@Produces({ MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON })
	public Collection<Credential> toCollection() {
		// TODO Auto-generated method stub
		logger.info("**** Debug to collection");
		return super.toCollection();
	}

	@GET
	@Path("/{id}")
	@Produces({ MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON })
	public Credential getById(@PathParam("id") Integer id) {
		// TODO Auto-generated method stub
		return super.getById(id);
	}

	@POST
	@Consumes({ MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON })
	@Produces({ MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON })
	public Collection<Credential> addIntoCollection(Credential credential) {
		super.add(credential);
		return super.toCollection();
	}

	@DELETE
	@Consumes({ MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON })
	@Produces({ MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON })
	public Collection<Credential> removeFromCollection(Credential credential) {
		super.remove(credential);
		return super.toCollection();
	}

	@DELETE
	@Path("/{id}")
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public void remove(@PathParam("id") Integer id) {
		Credential credential = super.getById(id);
		super.remove(credential);
	}

	@POST
	@Path("/new/{id}")
	@Produces({ MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON })
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public Credential createNewCredential(@PathParam("id") Integer id) {
		logger.info(" *** Create new credential *** ");
		Credential credential = new Credential(id);
		this.add(credential);
		return credential;
	}
	
	@PUT
	@Path("/{id}")
	@Consumes({ MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON })
	@Produces({ MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON })
	public Collection<Credential> addIntoCollection(@PathParam("id") Integer credentialId, Credential credentialInput) {
		Credential credential = super.getById(credentialId);
		credential.setUsername(credentialInput.getUsername());
		credential.setPassword(credentialInput.getPassword());
		this.add(credential);
		return super.toCollection();
	}
}
