package org.app.service.ejb;

import javax.ejb.Remote;

import org.app.patterns.EntityRepository;
import org.app.service.entities.Department;

@Remote
public interface DepartmentDataService extends EntityRepository<Department> {

}