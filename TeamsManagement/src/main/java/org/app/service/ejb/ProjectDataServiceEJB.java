package org.app.service.ejb;

import java.util.Collection;
import java.util.logging.Logger;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.app.patterns.EntityRepositoryBase;
import org.app.service.entities.Department;
import org.app.service.entities.Manager;
import org.app.service.entities.Project;
import org.app.service.entities.Project;

@Path("/projects")
@Stateless
@LocalBean
@SuppressWarnings("unused")
public class ProjectDataServiceEJB extends EntityRepositoryBase<Project> implements ProjectDataService {
	private static Logger logger = Logger.getLogger(ProjectDataServiceEJB.class.getName());

	@PersistenceContext(unitName = "MSD")
	private EntityManager em;

	@Override
	public void setEm(EntityManager em) {
		// TODO Auto-generated method stub
		super.setEm(em);
	}

	@Override
	public Project getById(Object id) {
		// TODO Auto-generated method stub
		return super.getById(id);
	}

	@Override
	public Collection<Project> get(Project entitySample) {
		// TODO Auto-generated method stub
		return super.get(entitySample);
	}

	@Override
	public Project[] toArray() {
		// TODO Auto-generated method stub
		return super.toArray();
	}

	@Override
	public Project add(Project entity) {
		// TODO Auto-generated method stub
		return super.add(entity);
	}

	@Override
	public Collection<Project> addAll(Collection<Project> entities) {
		// TODO Auto-generated method stub
		return super.addAll(entities);
	}

	@Override
	public boolean remove(Project entity) {
		// TODO Auto-generated method stub
		return super.remove(entity);
	}

	@Override
	public boolean removeAll(Collection<Project> entities) {
		// TODO Auto-generated method stub
		return super.removeAll(entities);
	}

	@Override
	public int size() {
		// TODO Auto-generated method stub
		return super.size();
	}

	@Override
	public Project refresh(Project entity) {
		// TODO Auto-generated method stub
		return super.refresh(entity);
	}

	@Override
	public Class<Project> getEntityParametrizedType() throws ClassCastException {
		// TODO Auto-generated method stub
		return super.getEntityParametrizedType();
	}

	@Override
	public int hashCode() {
		// TODO Auto-generated method stub
		return super.hashCode();
	}

	@Override
	public boolean equals(Object obj) {
		// TODO Auto-generated method stub
		return super.equals(obj);
	}

	@Override
	protected Object clone() throws CloneNotSupportedException {
		// TODO Auto-generated method stub
		return super.clone();
	}

	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return super.toString();
	}

	@Override
	protected void finalize() throws Throwable {
		// TODO Auto-generated method stub
		super.finalize();
	}

	@Override
	@GET
	@Produces({ MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON })
	public Collection<Project> toCollection() {
		// TODO Auto-generated method stub
		logger.info("**** Debug to collection");
		return super.toCollection();
	}

	@GET
	@Path("/{id}")
	@Produces({ MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON })
	public Project getById(@PathParam("id") Integer id) {
		// TODO Auto-generated method stub
		return super.getById(id);
	}

	@POST
	@Consumes({ MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON })
	@Produces({ MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON })
	public Collection<Project> addIntoCollection(Project project) {
		super.add(project);
		return super.toCollection();
	}

	@DELETE
	@Consumes({ MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON })
	@Produces({ MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON })
	public Collection<Project> removeFromCollection(Project project) {
		super.remove(project);
		return super.toCollection();
	}

	@DELETE
	@Path("/{id}")
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public void remove(@PathParam("id") Integer id) {
		Project project = super.getById(id);
		super.remove(project);
	}

	@POST
	@Path("/new/{id}")
	@Produces({ MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON })
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public Project createNewProject(@PathParam("id") Integer id) {
		logger.info(" *** Create new project *** ");
		Project project = new Project(id);
		this.add(project);
		return project;
	}
	
	@PUT
	@Path("/{id}")
	@Consumes({ MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON })
	@Produces({ MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON })
	public Collection<Project> addIntoCollection(@PathParam("id") Integer projectId, Project projectInput) {
		Project project = super.getById(projectId);
		project.setProjectName(projectInput.getProjectName());
		project.setProjectDescription(projectInput.getProjectDescription());
		this.add(project);
		return super.toCollection();
	}

}
