package org.app.service.ejb.aggregation;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.app.patterns.EntityRepository;
import org.app.patterns.EntityRepositoryBase;
import org.app.service.ejb.EmployeeDataService;
import org.app.service.entities.Department;
import org.app.service.entities.Project;

@Path("/departments")
@Stateless
@LocalBean
public class DepartmentProjectDataServiceEJB extends EntityRepositoryBase<Department>
		implements DepartmentProjectDataService {
	private static Logger logger = Logger.getLogger(DepartmentProjectDataServiceEJB.class.getName());

	@EJB
	private EmployeeDataService employeeService;
	private EntityRepository<Project> projectRepository;

	@PostConstruct
	public void init() {
		projectRepository = new EntityRepositoryBase<Project>(this.em, Project.class);
		logger.info("POSTCONSTRUCT-INIT: projectRepository:     " + this.projectRepository);
		logger.info("POSTCONSTRUCT-INIT: employeeService:     " + this.employeeService);
	}

	@POST
	@Path("/newDepartmentWithProject/{id}/{nrOfProjects}")
	@Produces({ MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON })
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	@Override
	public Department createNewDepartment(@PathParam("id") Integer departmentId,
			@PathParam("nrOfProjects") Integer nrOfProjects) {
		Department department = new Department(departmentId, "Department nr " + departmentId);
		List<Project> projectDepartment = new ArrayList<Project>();
		for (int i = 1; i <= nrOfProjects; i++) {
			Project thisProject = new Project(i, department, "Project nr " + i + " of department nr " + departmentId,
					"Project decription for project " + i, null);
			projectDepartment.add(thisProject);
		}
		department.setMyProjects(projectDepartment);
		this.add(department);
		return department;
	}

	@Override
	public Project getProjectById(Integer projectId) {
		return projectRepository.getById(projectId);
	}

}
