package org.app.service.ejb;

import java.util.Collection;
import java.util.logging.Logger;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.app.patterns.EntityRepositoryBase;
import org.app.service.entities.Manager;
import org.app.service.entities.Project;
import org.app.service.entities.Salary;

@Path("/salaries")
@Stateless
@LocalBean
@SuppressWarnings("unused")
public class SalaryDataServiceEJB extends EntityRepositoryBase<Salary> implements SalaryDataService {
	private static Logger logger = Logger.getLogger(SalaryDataServiceEJB.class.getName());

	@PersistenceContext(unitName = "MSD")
	private EntityManager em;

	@Override
	public void setEm(EntityManager em) {
		// TODO Auto-generated method stub
		super.setEm(em);
	}

	@Override
	public Salary getById(Object id) {
		// TODO Auto-generated method stub
		return super.getById(id);
	}

	@Override
	public Collection<Salary> get(Salary entitySample) {
		// TODO Auto-generated method stub
		return super.get(entitySample);
	}

	@Override
	public Salary[] toArray() {
		// TODO Auto-generated method stub
		return super.toArray();
	}

	@Override
	public Salary add(Salary entity) {
		// TODO Auto-generated method stub
		return super.add(entity);
	}

	@Override
	public Collection<Salary> addAll(Collection<Salary> entities) {
		// TODO Auto-generated method stub
		return super.addAll(entities);
	}

	@Override
	public boolean remove(Salary entity) {
		// TODO Auto-generated method stub
		return super.remove(entity);
	}

	@Override
	public boolean removeAll(Collection<Salary> entities) {
		// TODO Auto-generated method stub
		return super.removeAll(entities);
	}

	@Override
	public int size() {
		// TODO Auto-generated method stub
		return super.size();
	}

	@Override
	public Salary refresh(Salary entity) {
		// TODO Auto-generated method stub
		return super.refresh(entity);
	}

	@Override
	public Class<Salary> getEntityParametrizedType() throws ClassCastException {
		// TODO Auto-generated method stub
		return super.getEntityParametrizedType();
	}

	@Override
	public int hashCode() {
		// TODO Auto-generated method stub
		return super.hashCode();
	}

	@Override
	public boolean equals(Object obj) {
		// TODO Auto-generated method stub
		return super.equals(obj);
	}

	@Override
	protected Object clone() throws CloneNotSupportedException {
		// TODO Auto-generated method stub
		return super.clone();
	}

	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return super.toString();
	}

	@Override
	protected void finalize() throws Throwable {
		// TODO Auto-generated method stub
		super.finalize();
	}

	@Override
	@GET
	@Produces({ MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON })
	public Collection<Salary> toCollection() {
		// TODO Auto-generated method stub
		logger.info("**** Debug to collection");
		return super.toCollection();
	}

	@GET
	@Path("/{id}")
	@Produces({ MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON })
	public Salary getById(@PathParam("id") Integer id) {
		// TODO Auto-generated method stub
		return super.getById(id);
	}

	@POST
	@Consumes({ MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON })
	@Produces({ MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON })
	public Collection<Salary> addIntoCollection(Salary salary) {
		super.add(salary);
		return super.toCollection();
	}

	@DELETE
	@Consumes({ MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON })
	@Produces({ MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON })
	public Collection<Salary> removeFromCollection(Salary salary) {
		super.remove(salary);
		return super.toCollection();
	}

	@DELETE
	@Path("/{id}")
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public void remove(@PathParam("id") Integer id) {
		Salary salary = super.getById(id);
		super.remove(salary);
	}

	@POST
	@Path("/new/{id}")
	@Produces({ MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON })
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public Salary createNewSalary(@PathParam("id") Integer id) {
		logger.info(" *** Create new salary *** ");
		Salary salary = new Salary(id);
		this.add(salary);
		return salary;
	}
	
	@PUT
	@Path("/{id}")
	@Consumes({ MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON })
	@Produces({ MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON })
	public Collection<Salary> addIntoCollection(@PathParam("id") Integer salaryId, Salary salaryInput) {
		Salary salary = super.getById(salaryId);
		salary.setValue(salaryInput.getValue());
		this.add(salary);
		return super.toCollection();
	}
}
