package org.app.service.ejb;

import javax.ejb.Remote;

import org.app.patterns.EntityRepository;
import org.app.service.entities.Salary;

@Remote
public interface SalaryDataService extends EntityRepository<Salary> {

}