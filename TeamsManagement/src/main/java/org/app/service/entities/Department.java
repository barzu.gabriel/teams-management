package org.app.service.entities;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import org.app.service.rest.AtomLink;

@XmlRootElement(name = "department")
@XmlAccessorType(XmlAccessType.NONE)
@Entity
public class Department implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	private Integer departmentId;

	@Column(name = "departmentName")
	private String departmentName;

	@OneToOne(mappedBy = "department", cascade = CascadeType.REMOVE, fetch = FetchType.LAZY, orphanRemoval = true)
	private Manager myManager;

	@OneToMany(fetch = FetchType.EAGER, mappedBy = "department", cascade = CascadeType.REMOVE, orphanRemoval = true)
	private List<Project> myProjects;

	public Department() {
		super();
	}

	public Department(Integer departmentId) {
		this.departmentId = departmentId;
	}

	public Department(String departmentName) {
		this.departmentName = departmentName;
	}

	public Department(Integer departmentId, String departmentName) {
		this.departmentId = departmentId;
		this.departmentName = departmentName;
	}

	@XmlElement
	public Integer getDepartmentId() {
		return departmentId;
	}

	public void setDepartmentId(Integer departmentId) {
		this.departmentId = departmentId;
	}

	@XmlElement
	public String getDepartmentName() {
		return departmentName;
	}

	public void setDepartmentName(String departmentName) {
		this.departmentName = departmentName;
	}

//	@XmlElement
	public Manager getMyManager() {
		return myManager;
	}

	public void setMyManager(Manager myManager) {
		this.myManager = myManager;
	}

//	@XmlElementWrapper(name = "projects")
//	@XmlElement(name = "project")
	public List<Project> getMyProjects() {
		return myProjects;
	}

	public void setMyProjects(List<Project> myProjects) {
		this.myProjects = myProjects;
	}
	
//	@XmlElement(name = "nrOfProjects")
	public int getMyProjectsLength() {
		return myProjects.size();
	}

	public static String BASE_URL = "http://localhost:8080/TeamsManagement/rest/departments/";

	@XmlElement(name = "link")
	public AtomLink getLink() throws Exception {
		String restUrl = BASE_URL + this.getDepartmentId();
		return new AtomLink(restUrl, "get-department");
	}

}