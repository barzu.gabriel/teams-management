package org.app.service.entities;

import java.io.Serializable;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import org.app.service.rest.AtomLink;

@XmlRootElement(name = "project")
@XmlAccessorType(XmlAccessType.NONE)
@Entity
public class Project implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	private Integer projectId;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "departmentId", nullable = false)
	private Department department;

	@Column(name = "projectName")
	private String projectName;

	@Column(name = "projectDescription")
	private String projectDescription;

	@OneToMany(fetch = FetchType.EAGER, mappedBy = "project", cascade = CascadeType.REMOVE, orphanRemoval = true)
	private List<Employee> myEmployees;

	public Project() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Project(Integer projectId) {
		super();
		this.projectId = projectId;
	}

	public Project(Integer projectId, Department department, String projectName, String projectDescription,
			List<Employee> myEmployees) {
		super();
		this.projectId = projectId;
		this.department = department;
		this.projectName = projectName;
		this.projectDescription = projectDescription;
		this.myEmployees = myEmployees;
	}

	public Project(Department department, String projectName, String projectDescription, List<Employee> myEmployees) {
		super();
		this.department = department;
		this.projectName = projectName;
		this.projectDescription = projectDescription;
		this.myEmployees = myEmployees;
	}

	@XmlElement
	public Integer getProjectId() {
		return projectId;
	}

	public void setProjectId(Integer projectId) {
		this.projectId = projectId;
	}

	@XmlElement
	public Department getDepartment() {
		return department;
	}

	public void setDepartment(Department department) {
		this.department = department;
	}

	@XmlElement
	public String getProjectName() {
		return projectName;
	}

	public void setProjectName(String projectName) {
		this.projectName = projectName;
	}

	@XmlElement
	public String getProjectDescription() {
		return projectDescription;
	}

	public void setProjectDescription(String projectDescription) {
		this.projectDescription = projectDescription;
	}

//	@XmlElementWrapper(name = "employees")
//	@XmlElement(name = "employee")
	public List<Employee> getMyEmployees() {
		return myEmployees;
	}

	public void setMyEmployees(List<Employee> myEmployees) {
		this.myEmployees = myEmployees;
	}
	
//	@XmlElement(name = "nrOfEmployees")
	public int getMyEmployeesLength() {
		return myEmployees.size();
	}

	public static String BASE_URL = "http://localhost:8080/TeamsManagement/rest/projects/";

	@XmlElement(name = "link")
	public AtomLink getLink() throws Exception {
		String restUrl = BASE_URL + this.getProjectId();
		return new AtomLink(restUrl, "get-project");
	}
}