package org.app.service.entities;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import org.app.service.rest.AtomLink;

@XmlRootElement(name = "absence")
@XmlAccessorType(XmlAccessType.NONE)
@Entity
public class Absence implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	private Integer absenceId;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "personId", nullable = false)
	private Person person;

	@Column(name = "type")
	private String type;

	@Column(name = "startDate")
	private Date startDate;

	@Column(name = "endDate")
	private Date endDate;

	public Absence() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Absence(Integer absenceId) {
		super();
		this.absenceId = absenceId;
	}

	public Absence(Integer absenceId, Person person, String type, Date startDate, Date endDate) {
		super();
		this.absenceId = absenceId;
		this.person = person;
		this.type = type;
		this.startDate = startDate;
		this.endDate = endDate;
	}

	@XmlElement
	public Integer getAbsenceId() {
		return absenceId;
	}

	public void setAbsenceId(Integer absenceId) {
		this.absenceId = absenceId;
	}
	
	@XmlElement
	public Person getPerson() {
		return person;
	}

	public void setPerson(Person person) {
		this.person = person;
	}

	@XmlElement
	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	@XmlElement
	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	@XmlElement
	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public static String BASE_URL = "http://localhost:8080/TeamsManagement/rest/absences/";

	@XmlElement(name = "link")
	public AtomLink getLink() throws Exception {
		String restUrl = BASE_URL + this.getAbsenceId();
		return new AtomLink(restUrl, "get-absence");
	}
}