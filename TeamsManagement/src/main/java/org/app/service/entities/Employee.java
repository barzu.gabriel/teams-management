package org.app.service.entities;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import org.app.service.rest.AtomLink;

@XmlRootElement(name = "employee")
@XmlAccessorType(XmlAccessType.NONE)
@Entity
public class Employee extends Person implements Serializable {
	private static final long serialVersionUID = 1L;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "projectId", nullable = true)
	private Project project;

	public Employee() {
		super();
	}

	public Employee(Integer personId, String firstName, String lastName, String email, Date hiringDate,
			Credential myCredential, Salary mySalary, List<Absence> myAbsences, Project project) {
		super();
		this.personId = personId;
		this.firstName = firstName;
		this.lastName = lastName;
		this.email = email;
		this.hiringDate = hiringDate;
		this.myCredential = myCredential;
		this.mySalary = mySalary;
		this.myAbsences = myAbsences;
		this.project = project;
	}

	public Employee(Integer personId) {
		super();
		this.personId = personId;
	}

	public Employee(Project project) {
		super();
		this.project = project;
	}

	@XmlElement
	public Integer getPersonId() {
		return personId;
	}

	public void setPersonId(Integer personId) {
		this.personId = personId;
	}

	@XmlElement
	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	@XmlElement
	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	@XmlElement
	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	@XmlElement
	public Date getHiringDate() {
		return hiringDate;
	}

	public void setHiringDate(Date hiringDate) {
		this.hiringDate = hiringDate;
	}

	@XmlElement
	public Credential getMyCredential() {
		return myCredential;
	}

	public void setMyCredential(Credential myCredential) {
		this.myCredential = myCredential;
	}

	@XmlElement
	public Salary getMySalary() {
		return mySalary;
	}

	public void setMySalary(Salary mySalary) {
		this.mySalary = mySalary;
	}

//	@XmlElementWrapper(name = "absences")
//	@XmlElement(name = "absence")
	public List<Absence> getMyAbsences() {
		return myAbsences;
	}

	public void setMyAbsences(List<Absence> myAbsences) {
		this.myAbsences = myAbsences;
	}
	
//	@XmlElement(name = "nrOfAbsences")
	public int getMyAbsencesLength() {
		return myAbsences.size();
	}

	@XmlElement
	public Project getProject() {
		return project;
	}

	public void setProject(Project project) {
		this.project = project;
	}

	public static String BASE_URL = "http://localhost:8080/TeamsManagement/rest/employees/";

	@XmlElement(name = "link")
	public AtomLink getLink() throws Exception {
		String restUrl = BASE_URL + this.getPersonId();
		return new AtomLink(restUrl, "get-employee");
	}
}
