package org.app.service.entities;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import org.app.service.rest.AtomLink;

@XmlRootElement(name = "credential")
@XmlAccessorType(XmlAccessType.NONE)
@Entity
public class Credential implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	private Integer credentialId;

	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "personId", nullable = false)
	private Person person;

	@Column(name = "username")
	private String username;

	@Column(name = "password")
	private String password;

	public Credential() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Credential(Integer credentialId) {
		super();
		this.credentialId = credentialId;
	}

	public Credential(Person person, String username, String password) {
		super();
		this.person = person;
		this.username = username;
		this.password = password;
	}

	public Credential(Integer credentialId, Person person, String username, String password) {
		super();
		this.credentialId = credentialId;
		this.person = person;
		this.username = username;
		this.password = password;
	}

	@XmlElement
	public Integer getCredentialId() {
		return credentialId;
	}

	public void setCredentialId(Integer credentialId) {
		this.credentialId = credentialId;
	}

	public Person getPerson() {
		return person;
	}

	public void setPerson(Person person) {
		this.person = person;
	}

	@XmlElement
	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	@XmlElement
	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public static String BASE_URL = "http://localhost:8080/TeamsManagement/rest/credentials/";

	@XmlElement(name = "link")
	public AtomLink getLink() throws Exception {
		String restUrl = BASE_URL + this.getCredentialId();
		return new AtomLink(restUrl, "get-credential");
	}
}