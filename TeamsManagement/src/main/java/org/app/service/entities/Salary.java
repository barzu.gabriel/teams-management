package org.app.service.entities;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import org.app.service.rest.AtomLink;

@XmlRootElement(name = "salary")
@XmlAccessorType(XmlAccessType.NONE)
@Entity
public class Salary implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	private Integer salaryId;

	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "personId", nullable = false)
	private Person person;

	@Column(name = "value")
	private Double value;

	public Salary() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Salary(Integer salaryId) {
		super();
		this.salaryId = salaryId;
	}

	public Salary(Person person, Double value) {
		super();
		this.person = person;
		this.value = value;
	}

	public Salary(Integer salaryId, Person person, Double value) {
		super();
		this.salaryId = salaryId;
		this.person = person;
		this.value = value;
	}

	@XmlElement
	public Integer getSalaryId() {
		return salaryId;
	}

	public void setSalaryId(Integer salaryId) {
		this.salaryId = salaryId;
	}

	public Person getPerson() {
		return person;
	}

	public void setPerson(Person person) {
		this.person = person;
	}

	@XmlElement
	public Double getValue() {
		return value;
	}

	public void setValue(Double value) {
		this.value = value;
	}

	public static String BASE_URL = "http://localhost:8080/TeamsManagement/rest/salaries/";

	@XmlElement(name = "link")
	public AtomLink getLink() throws Exception {
		String restUrl = BASE_URL + this.getSalaryId();
		return new AtomLink(restUrl, "get-salary");
	}
}