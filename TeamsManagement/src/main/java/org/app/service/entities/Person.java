package org.app.service.entities;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

@Entity
@Inheritance(strategy = InheritanceType.TABLE_PER_CLASS)
public abstract class Person implements Serializable {
	private static final long serialVersionUID = 1L;
	@Id
	protected Integer personId;

	@Column(name = "firstName")
	protected String firstName;

	@Column(name = "lastName")
	protected String lastName;

	@Column(name = "email")
	protected String email;

	@Column(name = "hiringDate")
	protected Date hiringDate;

	@OneToOne(mappedBy = "person", cascade = CascadeType.REMOVE, fetch = FetchType.LAZY, orphanRemoval = true)
	protected Credential myCredential;

	@OneToOne(mappedBy = "person", cascade = CascadeType.REMOVE, fetch = FetchType.LAZY, orphanRemoval = true)
	protected Salary mySalary;

	@OneToMany(mappedBy = "person", fetch = FetchType.EAGER, cascade = CascadeType.REMOVE, orphanRemoval = true)
	protected List<Absence> myAbsences;

	public Person() {
		super();
		// TODO Auto-generated constructor stub
	}

}
