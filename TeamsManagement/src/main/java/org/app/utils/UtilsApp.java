package org.app.utils;

import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Random;

public class UtilsApp {
	public static String randomIdentifier() {
		String[] Beginning = { "Kr", "Ca", "Ra", "Mrok", "Cru", "Ray", "Bre", "Zed", "Drak", "Mor", "Jag", "Mer", "Jar",
				"Mjol", "Zork", "Mad", "Cry", "Zur", "Creo", "Azak", "Azur", "Rei", "Cro", "Mar", "Luk" };
		String[] Middle = { "air", "ir", "mi", "sor", "mee", "clo", "red", "cra", "ark", "arc", "miri", "lori", "cres",
				"mur", "zer", "marac", "zoir", "slamar", "salmar", "urak" };
		String[] End = { "d", "ed", "ark", "arc", "es", "er", "der", "tron", "med", "ure", "zur", "cred", "mur" };

		Random rand = new Random();

		return Beginning[rand.nextInt(Beginning.length)] + Middle[rand.nextInt(Middle.length)]
				+ End[rand.nextInt(End.length)];

	}

	public static int randomBetween(int min, int max) {
		Random rand = new Random();
		int randomNum = rand.nextInt((max - min) + 1) + min;
		return randomNum;
	}

	public static Date randomDate() {
		GregorianCalendar gc = new GregorianCalendar();
		int year = randomBetween(2005, 2017);
		gc.set(gc.YEAR, year);
		int dayOfYear = randomBetween(1, gc.getActualMaximum(gc.DAY_OF_YEAR));
		gc.set(gc.DAY_OF_YEAR, dayOfYear);
		Date date = gc.getTime();
		return date;
	}

	public static Double randomDouble() {
		Random r = new Random();
		Double min = 1000.0;
		Double max = 3000.0;
		Double random = min + r.nextFloat() * (max - min);
		return random;
	}
}