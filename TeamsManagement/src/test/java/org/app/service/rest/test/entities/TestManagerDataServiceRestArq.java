package org.app.service.rest.test.entities;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.Collection;
import java.util.logging.Logger;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MediaType;

import org.app.patterns.EntityRepository;
import org.app.service.ejb.ManagerDataService;
import org.app.service.entities.Manager;
import org.app.service.rest.ApplicationConfig;
import org.app.service.rest.AtomLink;
import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.shrinkwrap.api.Archive;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.EmptyAsset;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.junit.BeforeClass;
import org.junit.FixMethodOrder;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;

@FixMethodOrder
@RunWith(Arquillian.class)
public class TestManagerDataServiceRestArq {
	private static Logger logger = Logger.getLogger(TestManagerDataServiceRestArq.class.getName());
	private static String serviceURL = "http://localhost:8080/TeamsManagement/rest/managers";

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@Deployment
	public static Archive<?> createDeployment() {
		return ShrinkWrap.create(WebArchive.class, "msd-manager-rest-test.war").addPackage(Manager.class.getPackage())
				.addPackage(ManagerDataService.class.getPackage()).addPackage(EntityRepository.class.getPackage())
				.addPackage(AtomLink.class.getPackage()).addPackage(ApplicationConfig.class.getPackage())
				.addAsResource("META-INF/persistence.xml").addAsManifestResource(EmptyAsset.INSTANCE, "beans.xml");
	}

	@Test
	@Ignore
	public void test0_GetMessage() {
		String resourceURL = serviceURL + "/test";
		String response = ClientBuilder.newClient().target(resourceURL).request().get().readEntity(String.class);
		assertNotNull("Data service failed!", response);
	}

	@Test
	@Ignore
	public void test1_GetByID() {
		String resourceURL = serviceURL + "/1";
		logger.info("DEBUG: Junit TESTING: test1_GetByID ..." + resourceURL);
		Manager manager = ClientBuilder.newClient().target(resourceURL).request().get().readEntity(Manager.class);
		assertNotNull("REST Data Service failed!", manager);
		logger.info(">>>>>> DEBUG: REST Response ... " + manager);
	}

	@Test
	@Ignore
	public void test2_DeleteManager() {
		String resourceURL = serviceURL + "/";
		logger.info("DEBUG: Junit TESTING: test2_DeleteManager ...");
		Client client = ClientBuilder.newClient();
		Collection<Manager> managers = client.target(resourceURL).request().get()
				.readEntity(new GenericType<Collection<Manager>>() {
				});
		for (Manager d : managers) {
			client.target(resourceURL + d.getPersonId()).request().delete();
		}
		Collection<Manager> managersAfterDelete = client.target(resourceURL).request().get()
				.readEntity(new GenericType<Collection<Manager>>() {
				});
		assertTrue("Fail to read Managers!", managersAfterDelete.size() > 0);
	}

	@Test
	@Ignore
	public void test3_AddManager() {
		// addIntoCollection
		logger.info("DEBUG: Junit TESTING: test3_AddManager ...");
		Client client = ClientBuilder.newClient();
		Collection<Manager> managers;
		Integer managersToAdd = 3;
		Manager manager;
		for (int i = 1; i <= managersToAdd; i++) {
			manager = new Manager(i);
			managers = client.target(serviceURL).request().post(Entity.entity(manager, MediaType.APPLICATION_XML))
					.readEntity(new GenericType<Collection<Manager>>() {
					});
			assertTrue("Fail to read Managers!", managers.size() > 0);
		}
		managers = client.target(serviceURL).request().get().readEntity(new GenericType<Collection<Manager>>() {
		});
		assertTrue("Fail to add Managers!", managers.size() >= managersToAdd);
		managers.stream().forEach(System.out::println);
	}

	@Test
	@Ignore
	public void test4_GetManagers() {
		logger.info("DEBUG: Junit TESTING: test4_GetManagers ...");
		Collection<Manager> managers = ClientBuilder.newClient().target(serviceURL).request().get()
				.readEntity(new GenericType<Collection<Manager>>() {
				});
		assertTrue("Fail to read Managers!", managers.size() > 0);
	}

	@Test
	@Ignore
	public void test5_UpdateManager() {
		String resourceURL = serviceURL + "/1";
		Client client = ClientBuilder.newClient();
		Manager manager = client.target(resourceURL).request().accept(MediaType.APPLICATION_XML).get()
				.readEntity(Manager.class);
		assertNotNull("REST Data Service failed!", manager);
		manager.setContactPhone(manager.getContactPhone() + "_UPDATED");
		manager = client.target(resourceURL).request().accept(MediaType.APPLICATION_XML)
				.put(Entity.entity(manager, MediaType.APPLICATION_XML)).readEntity(Manager.class);
		assertNotNull("REST Data Service failed!", manager);
	}

}