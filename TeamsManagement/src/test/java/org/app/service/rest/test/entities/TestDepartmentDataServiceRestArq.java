package org.app.service.rest.test.entities;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.Collection;
import java.util.logging.Logger;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MediaType;

import org.app.patterns.EntityRepository;
import org.app.service.ejb.DepartmentDataService;
import org.app.service.entities.Department;
import org.app.service.rest.ApplicationConfig;
import org.app.service.rest.AtomLink;
import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.shrinkwrap.api.Archive;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.EmptyAsset;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.junit.BeforeClass;
import org.junit.FixMethodOrder;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;

@FixMethodOrder
@RunWith(Arquillian.class)
public class TestDepartmentDataServiceRestArq {
	private static Logger logger = Logger.getLogger(TestDepartmentDataServiceRestArq.class.getName());
	private static String serviceURL = "http://localhost:8080/TeamsManagement/rest/departments";

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@Deployment
	public static Archive<?> createDeployment() {
		return ShrinkWrap.create(WebArchive.class, "msd-department-rest-test.war")
				.addPackage(Department.class.getPackage()).addPackage(DepartmentDataService.class.getPackage())
				.addPackage(EntityRepository.class.getPackage()).addPackage(AtomLink.class.getPackage())
				.addPackage(ApplicationConfig.class.getPackage()).addAsResource("META-INF/persistence.xml")
				.addAsManifestResource(EmptyAsset.INSTANCE, "beans.xml");
	}

	@Test
	@Ignore
	public void test0_GetMessage() {
		String resourceURL = serviceURL + "/test";
		String response = ClientBuilder.newClient().target(resourceURL).request().get().readEntity(String.class);
		assertNotNull("Data service failed!", response);
	}

	@Test
	@Ignore
	public void test1_GetByID() {
		String resourceURL = serviceURL + "/1";
		logger.info("DEBUG: Junit TESTING: test1_GetByID ..." + resourceURL);
		Department department = ClientBuilder.newClient().target(resourceURL).request().get()
				.readEntity(Department.class);
		assertNotNull("REST Data Service failed!", department);
		logger.info(">>>>>> DEBUG: REST Response ... " + department);
	}

	@Test
	@Ignore
	public void test2_DeleteDepartment() {
		String resourceURL = serviceURL + "/";
		logger.info("DEBUG: Junit TESTING: test2_DeleteDepartment ...");
		Client client = ClientBuilder.newClient();
		Collection<Department> departments = client.target(resourceURL).request().get()
				.readEntity(new GenericType<Collection<Department>>() {
				});
		for (Department d : departments) {
			client.target(resourceURL + d.getDepartmentId()).request().delete();
		}
		Collection<Department> departmentsAfterDelete = client.target(resourceURL).request().get()
				.readEntity(new GenericType<Collection<Department>>() {
				});
		assertTrue("Fail to read Departments!", departmentsAfterDelete.size() > 0);
	}

	@Test
	@Ignore
	public void test3_AddDepartment() {
		// addIntoCollection
		logger.info("DEBUG: Junit TESTING: test3_AddDepartment ...");
		Client client = ClientBuilder.newClient();
		Collection<Department> departments;
		Integer departmentsToAdd = 3;
		Department department;
		for (int i = 1; i <= departmentsToAdd; i++) {
			department = new Department(i, "Department_" + (100 + i));
			departments = client.target(serviceURL).request().post(Entity.entity(department, MediaType.APPLICATION_XML))
					.readEntity(new GenericType<Collection<Department>>() {
					});
			assertTrue("Fail to read Departments!", departments.size() > 0);
		}
		departments = client.target(serviceURL).request().get().readEntity(new GenericType<Collection<Department>>() {
		});
		assertTrue("Fail to add Departments!", departments.size() >= departmentsToAdd);
		departments.stream().forEach(System.out::println);
	}

	@Test
	@Ignore
	public void test4_GetDepartments() {
		logger.info("DEBUG: Junit TESTING: test4_GetDepartments ...");
		Collection<Department> departments = ClientBuilder.newClient().target(serviceURL).request().get()
				.readEntity(new GenericType<Collection<Department>>() {
				});
		assertTrue("Fail to read Departments!", departments.size() > 0);
	}

	@Test
	@Ignore
	public void test5_UpdateDepartment() {
		String resourceURL = serviceURL + "/1";
		Client client = ClientBuilder.newClient();
		Department department = client.target(resourceURL).request().accept(MediaType.APPLICATION_XML).get()
				.readEntity(Department.class);
		assertNotNull("REST Data Service failed!", department);
		department.setDepartmentName(department.getDepartmentName() + "_UPDATED");
		department = client.target(resourceURL).request().accept(MediaType.APPLICATION_XML)
				.put(Entity.entity(department, MediaType.APPLICATION_XML)).readEntity(Department.class);
		assertNotNull("REST Data Service failed!", department);
	}

}