package org.app.service.rest.test.entities;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.Collection;
import java.util.logging.Logger;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MediaType;

import org.app.patterns.EntityRepository;
import org.app.service.ejb.ProjectDataService;
import org.app.service.entities.Project;
import org.app.service.rest.ApplicationConfig;
import org.app.service.rest.AtomLink;
import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.shrinkwrap.api.Archive;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.EmptyAsset;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.junit.BeforeClass;
import org.junit.FixMethodOrder;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;

@FixMethodOrder
@RunWith(Arquillian.class)
public class TestProjectDataServiceRestArq {
	private static Logger logger = Logger.getLogger(TestProjectDataServiceRestArq.class.getName());
	private static String serviceURL = "http://localhost:8080/TeamsManagement/rest/projects";

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@Deployment
	public static Archive<?> createDeployment() {
		return ShrinkWrap.create(WebArchive.class, "msd-project-rest-test.war").addPackage(Project.class.getPackage())
				.addPackage(ProjectDataService.class.getPackage()).addPackage(EntityRepository.class.getPackage())
				.addPackage(AtomLink.class.getPackage()).addPackage(ApplicationConfig.class.getPackage())
				.addAsResource("META-INF/persistence.xml").addAsManifestResource(EmptyAsset.INSTANCE, "beans.xml");
	}

	@Test
	@Ignore
	public void test0_GetMessage() {
		String resourceURL = serviceURL + "/test";
		String response = ClientBuilder.newClient().target(resourceURL).request().get().readEntity(String.class);
		assertNotNull("Data service failed!", response);
	}

	@Test
	@Ignore
	public void test1_GetByID() {
		String resourceURL = serviceURL + "/1";
		logger.info("DEBUG: Junit TESTING: test1_GetByID ..." + resourceURL);
		Project project = ClientBuilder.newClient().target(resourceURL).request().get().readEntity(Project.class);
		assertNotNull("REST Data Service failed!", project);
		logger.info(">>>>>> DEBUG: REST Response ... " + project);
	}

	@Test
	@Ignore
	public void test2_DeleteProject() {
		String resourceURL = serviceURL + "/";
		logger.info("DEBUG: Junit TESTING: test2_DeleteProject ...");
		Client client = ClientBuilder.newClient();
		Collection<Project> projects = client.target(resourceURL).request().get()
				.readEntity(new GenericType<Collection<Project>>() {
				});
		for (Project d : projects) {
			client.target(resourceURL + d.getProjectId()).request().delete();
		}
		Collection<Project> projectsAfterDelete = client.target(resourceURL).request().get()
				.readEntity(new GenericType<Collection<Project>>() {
				});
		assertTrue("Fail to read Projects!", projectsAfterDelete.size() > 0);
	}

	@Test
	@Ignore
	public void test3_AddProject() {
		// addIntoCollection
		logger.info("DEBUG: Junit TESTING: test3_AddProject ...");
		Client client = ClientBuilder.newClient();
		Collection<Project> projects;
		Integer projectsToAdd = 3;
		Project project;
		for (int i = 1; i <= projectsToAdd; i++) {
			project = new Project(i);
			projects = client.target(serviceURL).request().post(Entity.entity(project, MediaType.APPLICATION_XML))
					.readEntity(new GenericType<Collection<Project>>() {
					});
			assertTrue("Fail to read Projects!", projects.size() > 0);
		}
		projects = client.target(serviceURL).request().get().readEntity(new GenericType<Collection<Project>>() {
		});
		assertTrue("Fail to add Projects!", projects.size() >= projectsToAdd);
		projects.stream().forEach(System.out::println);
	}

	@Test
	@Ignore
	public void test4_GetProjects() {
		logger.info("DEBUG: Junit TESTING: test4_GetProjects ...");
		Collection<Project> projects = ClientBuilder.newClient().target(serviceURL).request().get()
				.readEntity(new GenericType<Collection<Project>>() {
				});
		assertTrue("Fail to read Projects!", projects.size() > 0);
	}

	@Test
	@Ignore
	public void test5_UpdateProject() {
		String resourceURL = serviceURL + "/1";
		Client client = ClientBuilder.newClient();
		Project project = client.target(resourceURL).request().accept(MediaType.APPLICATION_XML).get()
				.readEntity(Project.class);
		assertNotNull("REST Data Service failed!", project);
		project.setProjectName(project.getProjectName() + "_UPDATED");
		project = client.target(resourceURL).request().accept(MediaType.APPLICATION_XML)
				.put(Entity.entity(project, MediaType.APPLICATION_XML)).readEntity(Project.class);
		assertNotNull("REST Data Service failed!", project);
	}

}