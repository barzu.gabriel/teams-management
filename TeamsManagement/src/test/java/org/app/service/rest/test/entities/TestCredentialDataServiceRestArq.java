package org.app.service.rest.test.entities;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.Collection;
import java.util.logging.Logger;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MediaType;

import org.app.patterns.EntityRepository;
import org.app.service.ejb.CredentialDataService;
import org.app.service.entities.Credential;
import org.app.service.rest.ApplicationConfig;
import org.app.service.rest.AtomLink;
import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.shrinkwrap.api.Archive;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.EmptyAsset;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.junit.BeforeClass;
import org.junit.FixMethodOrder;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;

@FixMethodOrder
@RunWith(Arquillian.class)
public class TestCredentialDataServiceRestArq {
	private static Logger logger = Logger.getLogger(TestCredentialDataServiceRestArq.class.getName());
	private static String serviceURL = "http://localhost:8080/TeamsManagement/rest/credentials";

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@Deployment
	public static Archive<?> createDeployment() {
		return ShrinkWrap.create(WebArchive.class, "msd-credential-rest-test.war")
				.addPackage(Credential.class.getPackage()).addPackage(CredentialDataService.class.getPackage())
				.addPackage(EntityRepository.class.getPackage()).addPackage(AtomLink.class.getPackage())
				.addPackage(ApplicationConfig.class.getPackage()).addAsResource("META-INF/persistence.xml")
				.addAsManifestResource(EmptyAsset.INSTANCE, "beans.xml");
	}

	@Test
	@Ignore
	public void test0_GetMessage() {
		String resourceURL = serviceURL + "/test";
		String response = ClientBuilder.newClient().target(resourceURL).request().get().readEntity(String.class);
		assertNotNull("Data service failed!", response);
	}

	@Test
	@Ignore
	public void test1_GetByID() {
		String resourceURL = serviceURL + "/1";
		logger.info("DEBUG: Junit TESTING: test1_GetByID ..." + resourceURL);
		Credential credential = ClientBuilder.newClient().target(resourceURL).request().get()
				.readEntity(Credential.class);
		assertNotNull("REST Data Service failed!", credential);
		logger.info(">>>>>> DEBUG: REST Response ... " + credential);
	}

	@Test
	@Ignore
	public void test2_DeleteCredential() {
		String resourceURL = serviceURL + "/";
		logger.info("DEBUG: Junit TESTING: test2_DeleteCredential ...");
		Client client = ClientBuilder.newClient();
		Collection<Credential> credentials = client.target(resourceURL).request().get()
				.readEntity(new GenericType<Collection<Credential>>() {
				});
		for (Credential d : credentials) {
			client.target(resourceURL + d.getCredentialId()).request().delete();
		}
		Collection<Credential> credentialsAfterDelete = client.target(resourceURL).request().get()
				.readEntity(new GenericType<Collection<Credential>>() {
				});
		assertTrue("Fail to read Credentials!", credentialsAfterDelete.size() > 0);
	}

	@Test
	@Ignore
	public void test3_AddCredential() {
		// addIntoCollection
		logger.info("DEBUG: Junit TESTING: test3_AddCredential ...");
		Client client = ClientBuilder.newClient();
		Collection<Credential> credentials;
		Integer credentialsToAdd = 3;
		Credential credential;
		for (int i = 1; i <= credentialsToAdd; i++) {
			credential = new Credential(i);
			credentials = client.target(serviceURL).request().post(Entity.entity(credential, MediaType.APPLICATION_XML))
					.readEntity(new GenericType<Collection<Credential>>() {
					});
			assertTrue("Fail to read Credentials!", credentials.size() > 0);
		}
		credentials = client.target(serviceURL).request().get().readEntity(new GenericType<Collection<Credential>>() {
		});
		assertTrue("Fail to add Credentials!", credentials.size() >= credentialsToAdd);
		credentials.stream().forEach(System.out::println);
	}

	@Test
	@Ignore
	public void test4_GetCredentials() {
		logger.info("DEBUG: Junit TESTING: test4_GetCredentials ...");
		Collection<Credential> credentials = ClientBuilder.newClient().target(serviceURL).request().get()
				.readEntity(new GenericType<Collection<Credential>>() {
				});
		assertTrue("Fail to read Credentials!", credentials.size() > 0);
	}

	@Test
	@Ignore
	public void test5_UpdateCredential() {
		String resourceURL = serviceURL + "/1";
		Client client = ClientBuilder.newClient();
		Credential credential = client.target(resourceURL).request().accept(MediaType.APPLICATION_XML).get()
				.readEntity(Credential.class);
		assertNotNull("REST Data Service failed!", credential);
		credential.setPassword(credential.getPassword() + "_UPDATED");
		credential = client.target(resourceURL).request().accept(MediaType.APPLICATION_XML)
				.put(Entity.entity(credential, MediaType.APPLICATION_XML)).readEntity(Credential.class);
		assertNotNull("REST Data Service failed!", credential);
	}

}