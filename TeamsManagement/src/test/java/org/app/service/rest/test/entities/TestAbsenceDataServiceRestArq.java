package org.app.service.rest.test.entities;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.Collection;
import java.util.logging.Logger;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MediaType;

import org.app.patterns.EntityRepository;
import org.app.service.ejb.AbsenceDataService;
import org.app.service.entities.Absence;
import org.app.service.rest.ApplicationConfig;
import org.app.service.rest.AtomLink;
import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.shrinkwrap.api.Archive;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.EmptyAsset;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.junit.BeforeClass;
import org.junit.FixMethodOrder;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;

@FixMethodOrder
@RunWith(Arquillian.class)
public class TestAbsenceDataServiceRestArq {
	private static Logger logger = Logger.getLogger(TestAbsenceDataServiceRestArq.class.getName());
	private static String serviceURL = "http://localhost:8080/TeamsManagement/rest/absences";

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@Deployment
	public static Archive<?> createDeployment() {
		return ShrinkWrap.create(WebArchive.class, "msd-absence-rest-test.war").addPackage(Absence.class.getPackage())
				.addPackage(AbsenceDataService.class.getPackage()).addPackage(EntityRepository.class.getPackage())
				.addPackage(AtomLink.class.getPackage()).addPackage(ApplicationConfig.class.getPackage())
				.addAsResource("META-INF/persistence.xml").addAsManifestResource(EmptyAsset.INSTANCE, "beans.xml");
	}

	@Test
	@Ignore
	public void test0_GetMessage() {
		String resourceURL = serviceURL + "/test";
		String response = ClientBuilder.newClient().target(resourceURL).request().get().readEntity(String.class);
		assertNotNull("Data service failed!", response);
	}

	@Test
	@Ignore
	public void test1_GetByID() {
		String resourceURL = serviceURL + "/1";
		logger.info("DEBUG: Junit TESTING: test1_GetByID ..." + resourceURL);
		Absence absence = ClientBuilder.newClient().target(resourceURL).request().get().readEntity(Absence.class);
		assertNotNull("REST Data Service failed!", absence);
		logger.info(">>>>>> DEBUG: REST Response ... " + absence);
	}

	@Test
	@Ignore
	public void test2_DeleteAbsence() {
		String resourceURL = serviceURL + "/";
		logger.info("DEBUG: Junit TESTING: test2_DeleteAbsence ...");
		Client client = ClientBuilder.newClient();
		Collection<Absence> absences = client.target(resourceURL).request().get()
				.readEntity(new GenericType<Collection<Absence>>() {
				});
		for (Absence d : absences) {
			client.target(resourceURL + d.getAbsenceId()).request().delete();
		}
		Collection<Absence> absencesAfterDelete = client.target(resourceURL).request().get()
				.readEntity(new GenericType<Collection<Absence>>() {
				});
		assertTrue("Fail to read Absences!", absencesAfterDelete.size() > 0);
	}

	@Test
	@Ignore
	public void test3_AddAbsence() {
		// addIntoCollection
		logger.info("DEBUG: Junit TESTING: test3_AddAbsence ...");
		Client client = ClientBuilder.newClient();
		Collection<Absence> absences;
		Integer absencesToAdd = 3;
		Absence absence;
		for (int i = 1; i <= absencesToAdd; i++) {
			absence = new Absence(i);
			absences = client.target(serviceURL).request().post(Entity.entity(absence, MediaType.APPLICATION_XML))
					.readEntity(new GenericType<Collection<Absence>>() {
					});
			assertTrue("Fail to read Absences!", absences.size() > 0);
		}
		absences = client.target(serviceURL).request().get().readEntity(new GenericType<Collection<Absence>>() {
		});
		assertTrue("Fail to add Absences!", absences.size() >= absencesToAdd);
		absences.stream().forEach(System.out::println);
	}

	@Test
	@Ignore
	public void test4_GetAbsences() {
		logger.info("DEBUG: Junit TESTING: test4_GetAbsences ...");
		Collection<Absence> absences = ClientBuilder.newClient().target(serviceURL).request().get()
				.readEntity(new GenericType<Collection<Absence>>() {
				});
		assertTrue("Fail to read Absences!", absences.size() > 0);
	}

	@Test
	@Ignore
	public void test5_UpdateAbsence() {
		String resourceURL = serviceURL + "/1";
		Client client = ClientBuilder.newClient();
		Absence absence = client.target(resourceURL).request().accept(MediaType.APPLICATION_XML).get()
				.readEntity(Absence.class);
		assertNotNull("REST Data Service failed!", absence);
		absence.setType(absence.getType() + "_UPDATED");
		absence = client.target(resourceURL).request().accept(MediaType.APPLICATION_XML)
				.put(Entity.entity(absence, MediaType.APPLICATION_XML)).readEntity(Absence.class);
		assertNotNull("REST Data Service failed!", absence);
	}

}