package org.app.service.rest.test.entities;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.Collection;
import java.util.logging.Logger;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MediaType;

import org.app.patterns.EntityRepository;
import org.app.service.ejb.SalaryDataService;
import org.app.service.entities.Salary;
import org.app.service.rest.ApplicationConfig;
import org.app.service.rest.AtomLink;
import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.shrinkwrap.api.Archive;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.EmptyAsset;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.junit.BeforeClass;
import org.junit.FixMethodOrder;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;

@FixMethodOrder
@RunWith(Arquillian.class)
public class TestSalaryDataServiceRestArq {
	private static Logger logger = Logger.getLogger(TestSalaryDataServiceRestArq.class.getName());
	private static String serviceURL = "http://localhost:8080/TeamsManagement/rest/salarys";

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@Deployment
	public static Archive<?> createDeployment() {
		return ShrinkWrap.create(WebArchive.class, "msd-salary-rest-test.war").addPackage(Salary.class.getPackage())
				.addPackage(SalaryDataService.class.getPackage()).addPackage(EntityRepository.class.getPackage())
				.addPackage(AtomLink.class.getPackage()).addPackage(ApplicationConfig.class.getPackage())
				.addAsResource("META-INF/persistence.xml").addAsManifestResource(EmptyAsset.INSTANCE, "beans.xml");
	}

	@Test
	@Ignore
	public void test0_GetMessage() {
		String resourceURL = serviceURL + "/test";
		String response = ClientBuilder.newClient().target(resourceURL).request().get().readEntity(String.class);
		assertNotNull("Data service failed!", response);
	}

	@Test
	@Ignore
	public void test1_GetByID() {
		String resourceURL = serviceURL + "/1";
		logger.info("DEBUG: Junit TESTING: test1_GetByID ..." + resourceURL);
		Salary salary = ClientBuilder.newClient().target(resourceURL).request().get().readEntity(Salary.class);
		assertNotNull("REST Data Service failed!", salary);
		logger.info(">>>>>> DEBUG: REST Response ... " + salary);
	}

	@Test
	@Ignore
	public void test2_DeleteSalary() {
		String resourceURL = serviceURL + "/";
		logger.info("DEBUG: Junit TESTING: test2_DeleteSalary ...");
		Client client = ClientBuilder.newClient();
		Collection<Salary> salarys = client.target(resourceURL).request().get()
				.readEntity(new GenericType<Collection<Salary>>() {
				});
		for (Salary d : salarys) {
			client.target(resourceURL + d.getSalaryId()).request().delete();
		}
		Collection<Salary> salarysAfterDelete = client.target(resourceURL).request().get()
				.readEntity(new GenericType<Collection<Salary>>() {
				});
		assertTrue("Fail to read Salarys!", salarysAfterDelete.size() > 0);
	}

	@Test
	@Ignore
	public void test3_AddSalary() {
		// addIntoCollection
		logger.info("DEBUG: Junit TESTING: test3_AddSalary ...");
		Client client = ClientBuilder.newClient();
		Collection<Salary> salarys;
		Integer salarysToAdd = 3;
		Salary salary;
		for (int i = 1; i <= salarysToAdd; i++) {
			salary = new Salary(i);
			salarys = client.target(serviceURL).request().post(Entity.entity(salary, MediaType.APPLICATION_XML))
					.readEntity(new GenericType<Collection<Salary>>() {
					});
			assertTrue("Fail to read Salarys!", salarys.size() > 0);
		}
		salarys = client.target(serviceURL).request().get().readEntity(new GenericType<Collection<Salary>>() {
		});
		assertTrue("Fail to add Salarys!", salarys.size() >= salarysToAdd);
		salarys.stream().forEach(System.out::println);
	}

	@Test
	@Ignore
	public void test4_GetSalarys() {
		logger.info("DEBUG: Junit TESTING: test4_GetSalarys ...");
		Collection<Salary> salarys = ClientBuilder.newClient().target(serviceURL).request().get()
				.readEntity(new GenericType<Collection<Salary>>() {
				});
		assertTrue("Fail to read Salarys!", salarys.size() > 0);
	}

	@Test
	@Ignore
	public void test5_UpdateSalary() {
		String resourceURL = serviceURL + "/1";
		Client client = ClientBuilder.newClient();
		Salary salary = client.target(resourceURL).request().accept(MediaType.APPLICATION_XML).get()
				.readEntity(Salary.class);
		assertNotNull("REST Data Service failed!", salary);
		salary.setValue(salary.getValue() + 50);
		salary = client.target(resourceURL).request().accept(MediaType.APPLICATION_XML)
				.put(Entity.entity(salary, MediaType.APPLICATION_XML)).readEntity(Salary.class);
		assertNotNull("REST Data Service failed!", salary);
	}

}