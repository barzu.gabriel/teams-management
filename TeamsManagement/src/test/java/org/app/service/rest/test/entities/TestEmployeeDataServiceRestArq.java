package org.app.service.rest.test.entities;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.Collection;
import java.util.logging.Logger;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MediaType;

import org.app.patterns.EntityRepository;
import org.app.service.ejb.EmployeeDataService;
import org.app.service.entities.Employee;
import org.app.service.rest.ApplicationConfig;
import org.app.service.rest.AtomLink;
import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.shrinkwrap.api.Archive;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.EmptyAsset;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.junit.BeforeClass;
import org.junit.FixMethodOrder;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;

@FixMethodOrder
@RunWith(Arquillian.class)
public class TestEmployeeDataServiceRestArq {
	private static Logger logger = Logger.getLogger(TestEmployeeDataServiceRestArq.class.getName());
	private static String serviceURL = "http://localhost:8080/TeamsManagement/rest/employees";

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@Deployment
	public static Archive<?> createDeployment() {
		return ShrinkWrap.create(WebArchive.class, "msd-employee-rest-test.war").addPackage(Employee.class.getPackage())
				.addPackage(EmployeeDataService.class.getPackage()).addPackage(EntityRepository.class.getPackage())
				.addPackage(AtomLink.class.getPackage()).addPackage(ApplicationConfig.class.getPackage())
				.addAsResource("META-INF/persistence.xml").addAsManifestResource(EmptyAsset.INSTANCE, "beans.xml");
	}

	@Test
	@Ignore
	public void test0_GetMessage() {
		String resourceURL = serviceURL + "/test";
		String response = ClientBuilder.newClient().target(resourceURL).request().get().readEntity(String.class);
		assertNotNull("Data service failed!", response);
	}

	@Test
	@Ignore
	public void test1_GetByID() {
		String resourceURL = serviceURL + "/1";
		logger.info("DEBUG: Junit TESTING: test1_GetByID ..." + resourceURL);
		Employee employee = ClientBuilder.newClient().target(resourceURL).request().get().readEntity(Employee.class);
		assertNotNull("REST Data Service failed!", employee);
		logger.info(">>>>>> DEBUG: REST Response ... " + employee);
	}

	@Test
	@Ignore
	public void test2_DeleteEmployee() {
		String resourceURL = serviceURL + "/";
		logger.info("DEBUG: Junit TESTING: test2_DeleteEmployee ...");
		Client client = ClientBuilder.newClient();
		Collection<Employee> employees = client.target(resourceURL).request().get()
				.readEntity(new GenericType<Collection<Employee>>() {
				});
		for (Employee d : employees) {
			client.target(resourceURL + d.getPersonId()).request().delete();
		}
		Collection<Employee> employeesAfterDelete = client.target(resourceURL).request().get()
				.readEntity(new GenericType<Collection<Employee>>() {
				});
		assertTrue("Fail to read Employees!", employeesAfterDelete.size() > 0);
	}

	@Test
	@Ignore
	public void test3_AddEmployee() {
		// addIntoCollection
		logger.info("DEBUG: Junit TESTING: test3_AddEmployee ...");
		Client client = ClientBuilder.newClient();
		Collection<Employee> employees;
		Integer employeesToAdd = 3;
		Employee employee;
		for (int i = 1; i <= employeesToAdd; i++) {
			employee = new Employee(i);
			employees = client.target(serviceURL).request().post(Entity.entity(employee, MediaType.APPLICATION_XML))
					.readEntity(new GenericType<Collection<Employee>>() {
					});
			assertTrue("Fail to read Employees!", employees.size() > 0);
		}
		employees = client.target(serviceURL).request().get().readEntity(new GenericType<Collection<Employee>>() {
		});
		assertTrue("Fail to add Employees!", employees.size() >= employeesToAdd);
		employees.stream().forEach(System.out::println);
	}

	@Test
	@Ignore
	public void test4_GetEmployees() {
		logger.info("DEBUG: Junit TESTING: test4_GetEmployees ...");
		Collection<Employee> employees = ClientBuilder.newClient().target(serviceURL).request().get()
				.readEntity(new GenericType<Collection<Employee>>() {
				});
		assertTrue("Fail to read Employees!", employees.size() > 0);
	}

	@Test
	@Ignore
	public void test5_UpdateEmployee() {
		String resourceURL = serviceURL + "/1";
		Client client = ClientBuilder.newClient();
		Employee employee = client.target(resourceURL).request().accept(MediaType.APPLICATION_XML).get()
				.readEntity(Employee.class);
		assertNotNull("REST Data Service failed!", employee);
		employee.setEmail(employee.getEmail() + "_UPDATED");
		employee = client.target(resourceURL).request().accept(MediaType.APPLICATION_XML)
				.put(Entity.entity(employee, MediaType.APPLICATION_XML)).readEntity(Employee.class);
		assertNotNull("REST Data Service failed!", employee);
	}

}