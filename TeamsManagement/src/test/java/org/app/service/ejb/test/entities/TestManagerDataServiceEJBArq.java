package org.app.service.ejb.test.entities;

import static org.junit.Assert.assertEquals;

import java.util.Collection;
import java.util.logging.Logger;

import javax.ejb.EJB;

import org.app.patterns.EntityRepository;
import org.app.patterns.EntityRepositoryBase;
import org.app.service.ejb.ManagerDataService;
import org.app.service.ejb.ManagerDataServiceEJB;
import org.app.service.entities.Department;
import org.app.service.entities.Employee;
import org.app.service.entities.Manager;
import org.app.utils.UtilsApp;
import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.shrinkwrap.api.Archive;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.EmptyAsset;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.junit.BeforeClass;
import org.junit.FixMethodOrder;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;

@RunWith(Arquillian.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@SuppressWarnings("unused")
public class TestManagerDataServiceEJBArq {
	private static Logger logger = Logger.getLogger(TestManagerDataServiceEJBArq.class.getName());

	// Arquilian infrastructure
	@EJB
	private ManagerDataService service;

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@Deployment
	public static Archive<?> createDeployment() {
		return ShrinkWrap.create(WebArchive.class, "msd-manager-test.war").addPackage(Manager.class.getPackage())
				.addClass(ManagerDataService.class).addClass(ManagerDataServiceEJB.class)
				.addClass(EntityRepository.class).addClass(EntityRepositoryBase.class).addClass(UtilsApp.class)
				.addAsResource("META-INF/persistence.xml").addAsWebInfResource(EmptyAsset.INSTANCE, "beans.xml");
	}

	@Test
	@Ignore
	public void test_1_addManager() {
		logger.info("DEBUG: Junit TESTING ... add manager!");
		Collection<Manager> managersBeforeAdd = service.toCollection();
		Department depart = new Department(1);
		Manager nextManger = new Manager(1, UtilsApp.randomIdentifier(), UtilsApp.randomIdentifier(),
				UtilsApp.randomIdentifier() + "@domain.com", UtilsApp.randomDate(), null, null, null, "0754821311",
				depart);
		service.add(nextManger);
		Collection<Manager> managers = service.toCollection();
		assertEquals(managers.size(), managersBeforeAdd.size() + 1);
	}

}
