package org.app.service.ejb.test.entities;

import static org.junit.Assert.assertEquals;

import java.util.Collection;
import java.util.logging.Logger;

import javax.ejb.EJB;

import org.app.patterns.EntityRepository;
import org.app.patterns.EntityRepositoryBase;
import org.app.service.ejb.AbsenceDataService;
import org.app.service.ejb.AbsenceDataServiceEJB;
import org.app.service.entities.Absence;
import org.app.service.entities.Employee;
import org.app.utils.UtilsApp;
import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.shrinkwrap.api.Archive;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.EmptyAsset;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.junit.BeforeClass;
import org.junit.FixMethodOrder;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;

@RunWith(Arquillian.class)
@FixMethodOrder
@SuppressWarnings("unused")
public class TestAbsenceDataServiceEJBArq {
	private static Logger logger = Logger.getLogger(TestAbsenceDataServiceEJBArq.class.getName());

	// Arquilian infrastructure
	@EJB
	private AbsenceDataService service;

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@Deployment
	public static Archive<?> createDeployment() {
		return ShrinkWrap.create(WebArchive.class, "msd-absence-test.war").addPackage(Absence.class.getPackage())
				.addClass(AbsenceDataService.class).addClass(AbsenceDataServiceEJB.class)
				.addClass(EntityRepository.class).addClass(EntityRepositoryBase.class).addClass(UtilsApp.class)
				.addAsResource("META-INF/persistence.xml").addAsWebInfResource(EmptyAsset.INSTANCE, "beans.xml");
	}

	@Test
	@Ignore
	public void test_1_addAbsence() {
		logger.info("DEBUG: Junit TESTING ... addAbsence!");
		Collection<Absence> absenceBeforeAdd = service.toCollection();
		Employee emp1 = new Employee(1, UtilsApp.randomIdentifier(), UtilsApp.randomIdentifier(),
				UtilsApp.randomIdentifier() + "@domain.com", UtilsApp.randomDate(), null, null, null, null);
		Employee emp2 = new Employee(2, UtilsApp.randomIdentifier(), UtilsApp.randomIdentifier(),
				UtilsApp.randomIdentifier() + "@domain.com", UtilsApp.randomDate(), null, null, null, null);

		service.add(new Absence(1, emp1, "medicala", UtilsApp.randomDate(), UtilsApp.randomDate()));
		service.add(new Absence(1, emp2, "medicala", UtilsApp.randomDate(), UtilsApp.randomDate()));
		Collection<Absence> absences = service.toCollection();
		assertEquals(absences.size(), absenceBeforeAdd.size() + 2);
	}

}
