package org.app.service.ejb.test.entities;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.Collection;
import java.util.logging.Logger;

import javax.ejb.EJB;

import org.app.patterns.EntityRepository;
import org.app.patterns.EntityRepositoryBase;
import org.app.service.ejb.CredentialDataService;
import org.app.service.ejb.CredentialDataServiceEJB;
import org.app.service.entities.Credential;
import org.app.service.entities.Employee;
import org.app.utils.UtilsApp;
import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.shrinkwrap.api.Archive;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.EmptyAsset;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.junit.BeforeClass;
import org.junit.FixMethodOrder;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;

@RunWith(Arquillian.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@SuppressWarnings("unused")
public class TestCredentialDataServiceEJBArq {
	private static Logger logger = Logger.getLogger(TestCredentialDataServiceEJBArq.class.getName());

	// Arquilian infrastructure
	@EJB
	private CredentialDataService service;

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@Deployment
	public static Archive<?> createDeployment() {
		return ShrinkWrap.create(WebArchive.class, "msd-credential-test.war").addPackage(Credential.class.getPackage())
				.addClass(CredentialDataService.class).addClass(CredentialDataServiceEJB.class)
				.addClass(EntityRepository.class).addClass(EntityRepositoryBase.class).addClass(UtilsApp.class)
				.addAsResource("META-INF/persistence.xml").addAsWebInfResource(EmptyAsset.INSTANCE, "beans.xml");
	}

	@Test
	@Ignore
	public void test_1_addCredential() {
		logger.info("DEBUG: Junit TESTING ... add credentials!");
		Collection<Credential> credentialsBeforeAdd = service.toCollection();

		Employee emp1 = new Employee(1);
		Employee emp2 = new Employee(2);

		service.add(new Credential(1, emp1, UtilsApp.randomIdentifier(), UtilsApp.randomIdentifier()));
		service.add(new Credential(2, emp2, UtilsApp.randomIdentifier(), UtilsApp.randomIdentifier()));

		Collection<Credential> credentials = service.toCollection();
		assertEquals(credentials.size(), credentialsBeforeAdd.size() + 2);
	}

	@Test
	@Ignore
	public void test_2_deleteAllCredentials() {
		logger.info("DEBUG: Junit TESTING ... delete all credentials");
		Collection<Credential> credentials = service.toCollection();
		if (credentials.size() != 0) {
			for (Credential employee : credentials) {
				service.remove(employee);
			}
		}
		Collection<Credential> credentialsAfterDelete = service.toCollection();
		assertTrue("Failed to delete credentials", credentialsAfterDelete.size() == 0);
	}

}
