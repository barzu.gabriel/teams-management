package org.app.service.ejb.test.entities;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.Collection;
import java.util.logging.Logger;

import javax.ejb.EJB;

import org.app.patterns.EntityRepository;
import org.app.patterns.EntityRepositoryBase;
import org.app.service.ejb.SalaryDataService;
import org.app.service.ejb.SalaryDataServiceEJB;
import org.app.service.entities.Credential;
import org.app.service.entities.Employee;
import org.app.service.entities.Salary;
import org.app.utils.UtilsApp;
import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.shrinkwrap.api.Archive;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.EmptyAsset;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.junit.BeforeClass;
import org.junit.FixMethodOrder;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;

@RunWith(Arquillian.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@SuppressWarnings("unused")
public class TestSalaryDataServiceEJBArq {
	private static Logger logger = Logger.getLogger(TestSalaryDataServiceEJBArq.class.getName());

	// Arquilian infrastructure
	@EJB
	private SalaryDataService service;

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@Deployment
	public static Archive<?> createDeployment() {
		return ShrinkWrap.create(WebArchive.class, "msd-salary-test.war").addPackage(Salary.class.getPackage())
				.addClass(SalaryDataService.class).addClass(SalaryDataServiceEJB.class).addClass(EntityRepository.class)
				.addClass(EntityRepositoryBase.class).addAsResource("META-INF/persistence.xml")
				.addAsWebInfResource(EmptyAsset.INSTANCE, "beans.xml").addClass(UtilsApp.class);
	}

	@Test
	public void test_1_addSalary() {
		logger.info("DEBUG: Junit TESTING ... add salary!");
		Collection<Salary> salaryBeforeAdd = service.toCollection();

		Employee emp1 = new Employee(1);
		Employee emp2 = new Employee(2);

		service.add(new Salary(1, emp1, 4500.00));
		service.add(new Salary(2, emp2, 3600.00));

		Collection<Salary> salaries = service.toCollection();
		assertEquals(salaries.size(), salaryBeforeAdd.size() + 2);
	}

	@Test
	@Ignore
	public void test_2_deleteAllSalaries() {
		logger.info("DEBUG: Junit TESTING ... delete all salaries");
		Collection<Salary> salaries = service.toCollection();
		if (salaries.size() != 0) {
			for (Salary employee : salaries) {
				service.remove(employee);
			}
		}
		Collection<Salary> salariesAfterDelete = service.toCollection();
		assertTrue("Failed to delete credentials", salariesAfterDelete.size() == 0);
	}

}
