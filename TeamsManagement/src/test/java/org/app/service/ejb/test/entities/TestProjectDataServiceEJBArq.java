package org.app.service.ejb.test.entities;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.logging.Logger;

import javax.ejb.EJB;

import org.app.patterns.EntityRepository;
import org.app.patterns.EntityRepositoryBase;
import org.app.service.ejb.ProjectDataService;
import org.app.service.ejb.ProjectDataServiceEJB;
import org.app.service.entities.Department;
import org.app.service.entities.Project;
import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.shrinkwrap.api.Archive;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.EmptyAsset;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.junit.BeforeClass;
import org.junit.FixMethodOrder;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;

@RunWith(Arquillian.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@SuppressWarnings("unused")
public class TestProjectDataServiceEJBArq {
	private static Logger logger = Logger.getLogger(TestProjectDataServiceEJBArq.class.getName());

	// Arquilian infrastructure
	@EJB
	private ProjectDataService service;

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@Deployment
	public static Archive<?> createDeployment() {
		return ShrinkWrap.create(WebArchive.class, "msd-project-test.war").addPackage(Project.class.getPackage())
				.addClass(ProjectDataService.class).addClass(ProjectDataServiceEJB.class)
				.addClass(EntityRepository.class).addClass(EntityRepositoryBase.class)
				.addAsResource("META-INF/persistence.xml").addAsWebInfResource(EmptyAsset.INSTANCE, "beans.xml");
	}

	@Test
	@Ignore
	public void test_1_createNewProjects() {
		logger.info("DEBUG: Junit TESTING ... create new projects!");
		Collection<Project> projectsBeforeAdd = service.toCollection();

		Department dep1 = new Department(1);
		Department dep2 = new Department(2);

		service.add(new Project(1, dep1, "Project 1", "1-th project", null));
		service.add(new Project(2, dep1, "Project 2", "2-th project", null));
		service.add(new Project(3, dep2, "Project 3", "3-th project", null));

		Collection<Project> projects = service.toCollection();

		assertEquals(projects.size(), projectsBeforeAdd.size() + 3);
	}

	@Test
	@Ignore
	public void test_2_getProject() {
		logger.info("DEBUG: Junit TESTING ... get project!");
		Collection<Project> projects = service.toCollection();
		List<Project> listProjects = new ArrayList<Project>(projects);
		int listProjectsSize = listProjects.size() - 1;
		int lastProjectIndex = listProjects.get(listProjectsSize).getProjectId();
		assertTrue("Failed to get department", service.getById(lastProjectIndex).getProjectId() == lastProjectIndex);
	}

	@Test
	@Ignore
	public void test_3_deleteAllProjects() {
		logger.info("DEBUG: Junit TESTING ... delete all projects!");
		Collection<Project> projects = service.toCollection();
		if (projects.size() != 0) {
			for (Project depart : projects) {
				service.remove(depart);
			}
		}
		Collection<Project> projectsAfterDelete = service.toCollection();
		assertTrue("Failed to delete projects", projectsAfterDelete.size() == 0);
	}

}
