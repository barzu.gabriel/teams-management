package org.app.service.ejb.test.entities;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.logging.Logger;

import javax.ejb.EJB;

import org.app.patterns.EntityRepository;
import org.app.patterns.EntityRepositoryBase;
import org.app.service.ejb.DepartmentDataService;
import org.app.service.ejb.DepartmentDataServiceEJB;
import org.app.service.entities.Department;
import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.shrinkwrap.api.Archive;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.EmptyAsset;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.junit.BeforeClass;
import org.junit.FixMethodOrder;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;

@RunWith(Arquillian.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class TestDepartmentDataServiceEJBArq {
	private static Logger logger = Logger.getLogger(TestDepartmentDataServiceEJBArq.class.getName());

	// Arquilian infrastructure
	@EJB
	private DepartmentDataService service;

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@Deployment
	public static Archive<?> createDeployment() {
		return ShrinkWrap.create(WebArchive.class, "msd-department-test.war").addPackage(Department.class.getPackage())
				.addClass(DepartmentDataService.class).addClass(DepartmentDataServiceEJB.class)
				.addClass(EntityRepository.class).addClass(EntityRepositoryBase.class)
				.addAsResource("META-INF/persistence.xml").addAsWebInfResource(EmptyAsset.INSTANCE, "beans.xml");
	}

	@Test
	@Ignore
	public void test_1_createNewDeparments() {
		logger.info("DEBUG: Junit TESTING ... create new deparments!");
		Collection<Department> departmentsBeforeAdd = service.toCollection();
		int lastDepartmentSize = departmentsBeforeAdd.size();
		int nrOfDepartments = 2;
		for (int i = 0; i < nrOfDepartments; i++) {
			service.add(new Department(lastDepartmentSize + 1 + i, "Department nr " + (lastDepartmentSize + 1 + i)));
		}
		Collection<Department> departments = service.toCollection();
		assertEquals(departments.size(), departmentsBeforeAdd.size() + nrOfDepartments);
	}

	@Test
	@Ignore
	public void test_2_getDeparment() {
		logger.info("DEBUG: Junit TESTING ... get deparment!");
		Collection<Department> departments = service.toCollection();
		List<Department> listDepartments = new ArrayList<Department>(departments);
		int listDepartmentsSize = listDepartments.size() - 1;
		int lastDepartmentIndex = listDepartments.get(listDepartmentsSize).getDepartmentId();
		assertTrue("Failed to get department",
				service.getById(lastDepartmentIndex).getDepartmentId() == lastDepartmentIndex);
	}

	@Test
	@Ignore
	public void test_3_deleteAllDepartments() {
		logger.info("DEBUG: Junit TESTING ... delete all departments!");
		Collection<Department> departments = service.toCollection();
		if (departments.size() != 0) {
			for (Department depart : departments) {
				service.remove(depart);
			}
		}
		Collection<Department> departmentsAfterDelete = service.toCollection();
		assertTrue("Failed to delete departments", departmentsAfterDelete.size() == 0);
	}

}
