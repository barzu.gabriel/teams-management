package org.app.service.ejb.test.dbPopulate;

import javax.ejb.EJB;

import org.app.patterns.EntityRepository;
import org.app.patterns.EntityRepositoryBase;
import org.app.service.ejb.AbsenceDataService;
import org.app.service.ejb.AbsenceDataServiceEJB;
import org.app.service.ejb.CredentialDataService;
import org.app.service.ejb.CredentialDataServiceEJB;
import org.app.service.ejb.DepartmentDataService;
import org.app.service.ejb.DepartmentDataServiceEJB;
import org.app.service.ejb.EmployeeDataService;
import org.app.service.ejb.EmployeeDataServiceEJB;
import org.app.service.ejb.ManagerDataService;
import org.app.service.ejb.ManagerDataServiceEJB;
import org.app.service.ejb.ProjectDataService;
import org.app.service.ejb.ProjectDataServiceEJB;
import org.app.service.ejb.SalaryDataService;
import org.app.service.ejb.SalaryDataServiceEJB;
import org.app.service.entities.Absence;
import org.app.service.entities.Credential;
import org.app.service.entities.Department;
import org.app.service.entities.Employee;
import org.app.service.entities.Manager;
import org.app.service.entities.Project;
import org.app.service.entities.Salary;
import org.app.service.rest.AtomLink;
import org.app.utils.UtilsApp;
import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.shrinkwrap.api.Archive;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.EmptyAsset;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;

@RunWith(Arquillian.class)
@FixMethodOrder
public class TestDBPopulateDataServiceEJBArq {

	@Deployment
	public static Archive<?> createDeployment() {
		return ShrinkWrap.create(WebArchive.class, "msd-test.war")
				// AtomLink
				.addPackage(AtomLink.class.getPackage())
				// department
				.addPackage(Department.class.getPackage()).addClass(DepartmentDataService.class)
				.addClass(DepartmentDataServiceEJB.class)
				// project
				.addPackage(Project.class.getPackage()).addClass(ProjectDataService.class)
				.addClass(ProjectDataServiceEJB.class)
				// employee
				.addPackage(Employee.class.getPackage()).addClass(EmployeeDataService.class)
				.addClass(EmployeeDataServiceEJB.class)
				// credential
				.addPackage(Credential.class.getPackage()).addClass(CredentialDataService.class)
				.addClass(CredentialDataServiceEJB.class)
				// salary
				.addPackage(Salary.class.getPackage()).addClass(SalaryDataService.class)
				.addClass(SalaryDataServiceEJB.class)
				// absence
				.addPackage(Absence.class.getPackage()).addClass(AbsenceDataService.class)
				.addClass(AbsenceDataServiceEJB.class)
				// manager
				.addPackage(Manager.class.getPackage()).addClass(ManagerDataService.class)
				.addClass(ManagerDataServiceEJB.class)
				// entity repo
				.addClass(EntityRepository.class).addClass(EntityRepositoryBase.class).addClass(UtilsApp.class)
				.addAsResource("META-INF/persistence.xml").addAsWebInfResource(EmptyAsset.INSTANCE, "beans.xml");
	}

	@EJB
	private DepartmentDataService departmentService;

	@EJB
	private ProjectDataService projectService;

	@EJB
	private EmployeeDataService employeeService;

	@EJB
	private CredentialDataService credentialService;

	@EJB
	private SalaryDataService salaryService;

	@EJB
	private AbsenceDataService absenceService;

	@EJB
	private ManagerDataService managerService;

	@Test
	public void populate_db() {
		// adding department 1
		Department dept1 = new Department(1, "Department nr 1");

		// adding man 1
		String man1FirstName = UtilsApp.randomIdentifier();
		String man1LastName = UtilsApp.randomIdentifier();
		Credential credMan1 = new Credential(1, null, man1FirstName.toLowerCase() + "." + man1LastName.toLowerCase(),
				"parola1");
		Salary slrMan1 = new Salary(1, null, 3500.0);
		Manager man1 = new Manager(1, man1FirstName, man1LastName, man1FirstName + "." + man1LastName + "@yahoo.com",
				UtilsApp.randomDate(), credMan1, slrMan1, null, "0754790270", dept1);
		credMan1.setPerson(man1);
		slrMan1.setPerson(man1);

		// adding project 1
		Project prj1 = new Project(1, dept1, "Project nr 1", "Description for project nr 1", null);

		// adding emp 1
		String emp1FirstName = UtilsApp.randomIdentifier();
		String emp1LastName = UtilsApp.randomIdentifier();
		Credential credEmp1 = new Credential(2, null, emp1FirstName.toLowerCase() + "." + emp1LastName.toLowerCase(),
				"parola2");
		Salary slrEmp1 = new Salary(2, null, 1500.0);
		Employee emp1 = new Employee(2, emp1FirstName, emp1LastName, emp1FirstName + "." + emp1LastName + "@yahoo.com",
				UtilsApp.randomDate(), null, null, null, prj1);
		credEmp1.setPerson(emp1);
		slrEmp1.setPerson(emp1);

		// adding emp 2
		String emp2FirstName = UtilsApp.randomIdentifier();
		String emp2LastName = UtilsApp.randomIdentifier();
		Credential credEmp2 = new Credential(3, null, emp2FirstName.toLowerCase() + "." + emp2LastName.toLowerCase(),
				"parola2");
		Salary slrEmp2 = new Salary(3, null, 1800.0);
		Employee emp2 = new Employee(3, emp2FirstName, emp2LastName,
				emp2FirstName.toLowerCase() + "." + emp2LastName.toLowerCase() + "@yahoo.com", UtilsApp.randomDate(),
				null, null, null, prj1);
		credEmp2.setPerson(emp2);
		slrEmp2.setPerson(emp2);

		// adding emp 3
		String emp3FirstName = UtilsApp.randomIdentifier();
		String emp3LastName = UtilsApp.randomIdentifier();
		Credential credEmp3 = new Credential(4, null, emp3FirstName.toLowerCase() + "." + emp3LastName.toLowerCase(),
				"parola3");
		Salary slrEmp3 = new Salary(4, null, 800.0);
		Employee emp3 = new Employee(4, emp3FirstName, emp3LastName,
				emp3FirstName.toLowerCase() + "." + emp3LastName.toLowerCase() + "@yahoo.com", UtilsApp.randomDate(),
				null, null, null, prj1);
		credEmp3.setPerson(emp3);
		slrEmp3.setPerson(emp3);

		departmentService.add(dept1);
		projectService.add(prj1);
		employeeService.add(emp1);
		managerService.add(man1);
		employeeService.add(emp2);
		employeeService.add(emp3);
		credentialService.add(credMan1);
		credentialService.add(credEmp1);
		credentialService.add(credEmp2);
		credentialService.add(credEmp3);
		salaryService.add(slrMan1);
		salaryService.add(slrEmp1);
		salaryService.add(slrEmp2);
		salaryService.add(slrEmp3);

		// adding project 2
		Project prj2 = new Project(2, dept1, "Project nr 2", "Description for project nr 2", null);

		// adding emp 4
		String emp4FirstName = UtilsApp.randomIdentifier();
		String emp4LastName = UtilsApp.randomIdentifier();
		Credential credEmp4 = new Credential(5, null, emp4FirstName.toLowerCase() + "." + emp4LastName.toLowerCase(),
				"parola4");
		Salary slrEmp4 = new Salary(5, null, 2000.0);
		Employee emp4 = new Employee(5, emp4FirstName, emp4LastName,
				emp4FirstName.toLowerCase() + "." + emp4LastName.toLowerCase() + "@yahoo.com", UtilsApp.randomDate(),
				null, null, null, prj2);
		credEmp4.setPerson(emp4);
		slrEmp4.setPerson(emp4);

		// adding emp 5
		String emp5FirstName = UtilsApp.randomIdentifier();
		String emp5LastName = UtilsApp.randomIdentifier();
		Credential credEmp5 = new Credential(6, null, emp5FirstName.toLowerCase() + "." + emp5LastName.toLowerCase(),
				"parola5");
		Salary slrEmp5 = new Salary(6, null, 1900.0);
		Employee emp5 = new Employee(6, emp5FirstName, emp5LastName,
				emp5FirstName.toLowerCase() + "." + emp5LastName.toLowerCase() + "@yahoo.com", UtilsApp.randomDate(),
				null, null, null, prj2);
		credEmp5.setPerson(emp5);
		slrEmp5.setPerson(emp5);

		projectService.add(prj2);
		employeeService.add(emp4);
		employeeService.add(emp5);
		credentialService.add(credEmp4);
		credentialService.add(credEmp5);
		salaryService.add(slrEmp4);
		salaryService.add(slrEmp5);

		// adding department 2
		Department dept2 = new Department(2, "Department nr 2");

		// adding manager for department 2
		String man2FirstName = UtilsApp.randomIdentifier();
		String man21LastName = UtilsApp.randomIdentifier();
		Credential credMan2 = new Credential(7, null, man2FirstName.toLowerCase() + "." + man21LastName.toLowerCase(),
				"parola1");
		Salary slrMan2 = new Salary(7, null, 3500.0);
		Manager man2 = new Manager(7, man2FirstName, man21LastName, man2FirstName + "." + man21LastName + "@yahoo.com",
				UtilsApp.randomDate(), credMan2, slrMan2, null, "0754821311", dept2);
		credMan2.setPerson(man2);
		slrMan2.setPerson(man2);

		// adding project 1
		Project prj3 = new Project(3, dept2, "Project nr 3", "Description for project nr 3", null);

		// adding emp 6
		String emp6FirstName = UtilsApp.randomIdentifier();
		String emp6LastName = UtilsApp.randomIdentifier();
		Credential credEmp6 = new Credential(8, null, emp6FirstName.toLowerCase() + "." + emp6LastName.toLowerCase(),
				"parola6");
		Salary slrEmp6 = new Salary(8, null, 2500.0);
		Employee emp6 = new Employee(8, emp6FirstName, emp6LastName,
				emp6FirstName.toLowerCase() + "." + emp6LastName.toLowerCase() + "@yahoo.com", UtilsApp.randomDate(),
				null, null, null, prj3);
		credEmp6.setPerson(emp6);
		slrEmp6.setPerson(emp6);

		departmentService.add(dept2);
		managerService.add(man2);
		projectService.add(prj3);
		employeeService.add(emp6);
		credentialService.add(credEmp6);
		salaryService.add(slrEmp6);
	}
}
