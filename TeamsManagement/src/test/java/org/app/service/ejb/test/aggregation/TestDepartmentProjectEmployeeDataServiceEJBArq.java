package org.app.service.ejb.test.aggregation;

import static org.junit.Assert.assertNotNull;

import java.util.List;
import java.util.logging.Logger;

import javax.ejb.EJB;

import org.app.patterns.EntityRepository;
import org.app.service.ejb.EmployeeDataService;
import org.app.service.ejb.EmployeeDataServiceEJB;
import org.app.service.ejb.aggregation.DepartmentProjectDataService;
import org.app.service.ejb.aggregation.DepartmentProjectDataServiceEJB;
import org.app.service.entities.Department;
import org.app.service.entities.Project;
import org.app.service.rest.AtomLink;
import org.app.utils.UtilsApp;
import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.shrinkwrap.api.Archive;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.EmptyAsset;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;

@RunWith(Arquillian.class)
@FixMethodOrder
public class TestDepartmentProjectEmployeeDataServiceEJBArq {
	private static Logger logger = Logger.getLogger(TestDepartmentProjectEmployeeDataServiceEJBArq.class.getName());

	@Deployment
	public static Archive<?> createDeployment() {
		return ShrinkWrap.create(WebArchive.class, "msd-departmentProjectEmployee-test.war")
				.addPackage(AtomLink.class.getPackage()).addPackage(EntityRepository.class.getPackage())
				.addPackage(Department.class.getPackage()).addClass(EmployeeDataService.class)
				.addClass(EmployeeDataServiceEJB.class).addClass(DepartmentProjectDataService.class)
				.addClass(DepartmentProjectDataServiceEJB.class).addClass(UtilsApp.class)
				.addAsResource("META-INF/persistence.xml").addAsWebInfResource(EmptyAsset.INSTANCE, "beans.xml");
	}

	@EJB
	private static DepartmentProjectDataService service;

	@Test
	public void test_1_createNewDepartment() {
		logger.info("DEBUG: Junit TESTING ... createNewDepartment!");
		Department department = service.createNewDepartment(1, 2);
		assertNotNull("Fail to create a new department in repository", department.toString());

//		department.setDepartmentName(department.getDepartmentName() + " - changed by test");
//		List<Project> projectDepartment = department.getMyProjects();
//		for (Project proj : projectDepartment) {
//			proj.setProjectName(proj.getProjectName() + " - changed by test");
//			// projectDepartment.add(proj);
//		}
//		department.setMyProjects(projectDepartment);
//		department = service.add(department);
//		assertNotNull("Fail to save new department in repository", department.toString());
//		logger.info("DEBUG: createNewDepartment : department changed : " + department.toString());
//
//		department = service.getById(1);
//		assertNotNull("Fail to find department in repository", department);
//		logger.info("DEBUG: createNewDepartment : queried changed : " + department.toString());
	}
}
