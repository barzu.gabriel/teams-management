package org.app.service.ejb.test.entities;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.Collection;
import java.util.logging.Logger;

import javax.ejb.EJB;

import org.app.patterns.EntityRepository;
import org.app.patterns.EntityRepositoryBase;
import org.app.service.ejb.EmployeeDataService;
import org.app.service.ejb.EmployeeDataServiceEJB;
import org.app.service.entities.Employee;
import org.app.service.entities.Project;
import org.app.utils.UtilsApp;
import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.shrinkwrap.api.Archive;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.EmptyAsset;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.junit.BeforeClass;
import org.junit.FixMethodOrder;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;

@RunWith(Arquillian.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
@SuppressWarnings("unused")
public class TestEmployeeDataServiceEJBArq {
	private static Logger logger = Logger.getLogger(TestEmployeeDataServiceEJBArq.class.getName());

	// Arquilian infrastructure
	@EJB
	private EmployeeDataService service;

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@Deployment
	public static Archive<?> createDeployment() {
		return ShrinkWrap.create(WebArchive.class, "msd-employee-test.war").addPackage(Employee.class.getPackage())
				.addClass(EmployeeDataService.class).addClass(EmployeeDataServiceEJB.class)
				.addClass(EntityRepository.class).addClass(EntityRepositoryBase.class).addClass(UtilsApp.class)
				.addAsResource("META-INF/persistence.xml").addAsWebInfResource(EmptyAsset.INSTANCE, "beans.xml");
	}

	@Test
	public void test_1_addEmployees() {
		logger.info("DEBUG: Junit TESTING ... add employees");
		Collection<Employee> employeesBeforeAdd = service.toCollection();
		Project prj1 = new Project(1);
		int nrOfEmployeePrj1 = 3;
		for (int i = 0; i < nrOfEmployeePrj1; i++) {
			service.add(new Employee(i, UtilsApp.randomIdentifier(), UtilsApp.randomIdentifier(),
					UtilsApp.randomIdentifier() + "@domain.com", UtilsApp.randomDate(), null, null, null, prj1));
		}
		Project prj2 = new Project(2);
		int nrOfEmployeePrj2 = 3;
		for (int i = 0; i < nrOfEmployeePrj2; i++) {
			service.add(new Employee(i, UtilsApp.randomIdentifier(), UtilsApp.randomIdentifier(),
					UtilsApp.randomIdentifier() + "@domain.com", UtilsApp.randomDate(), null, null, null, prj2));
		}
		Project pr3 = new Project(3);
		int nrOfEmployeePrj3 = 4;
		for (int i = 0; i < nrOfEmployeePrj3; i++) {
			service.add(new Employee(i, UtilsApp.randomIdentifier(), UtilsApp.randomIdentifier(),
					UtilsApp.randomIdentifier() + "@domain.com", UtilsApp.randomDate(), null, null, null, pr3));
		}
		Collection<Employee> employees = service.toCollection();
		assertEquals(employees.size(),
				employeesBeforeAdd.size() + nrOfEmployeePrj1 + nrOfEmployeePrj2 + nrOfEmployeePrj3);
	}

	@Test
	@Ignore
	public void test_2_closeCompany() {
		logger.info("DEBUG: Junit TESTING ... close company");
		Collection<Employee> employees = service.toCollection();
		if (employees.size() != 0) {
			for (Employee emp : employees) {
				service.remove(emp);
			}
		}
		Collection<Employee> employeesAfterDelete = service.toCollection();
		assertTrue("Failed to close company", employeesAfterDelete.size() == 0);
	}

}
