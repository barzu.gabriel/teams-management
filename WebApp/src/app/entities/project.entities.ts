export class Project {
    projectId: number;
    projectName: string;
    projectDescription: string;
    department: any;
}