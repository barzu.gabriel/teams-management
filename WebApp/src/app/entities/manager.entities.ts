import { Person } from './person.entities'

export class Manager extends Person {
    contactPhone: String;
    department: any;
}