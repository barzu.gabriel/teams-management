export class Person {
    personId: number;
    firstName: string;
    lastName: string;
    email: string;
    hiringDate: Date;
}