import { Injectable } from '@angular/core';
import { Http, Response, Headers, URLSearchParams, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

import { Manager } from '../entities/manager.entities';

@Injectable()
export class ManagerService {

    //URL for CRUD operations
    managerUrl = "http://localhost:8080/TeamsManagement/rest/managers";

    //Create constructor to get Http instance
    constructor(private http: Http) {
    }

    //Fetch all managers
    getAllManagers(): Observable<Manager[]> {
        return this.http.get(this.managerUrl)
            .map(this.extractData)
            .catch(this.handleError);

    }

    //Create manager
    createManager(manager: Manager): Observable<number> {
        let cpHeaders = new Headers({ 'Content-Type': 'application/json' });
        let options = new RequestOptions({ headers: cpHeaders });
        return this.http.post(this.managerUrl, manager, options)
            .map(success => success.status)
            .catch(this.handleError);
    }

    //Fetch manager by id
    getManagerById(managerId: string): Observable<Manager> {
        let cpHeaders = new Headers({ 'Content-Type': 'application/json' });
        let options = new RequestOptions({ headers: cpHeaders });
        console.log(this.managerUrl + "/" + managerId);
        return this.http.get(this.managerUrl + "/" + managerId)
            .map(this.extractData)
            .catch(this.handleError);
    }

    //Update manager
    updateManager(manager: Manager): Observable<number> {
        let cpHeaders = new Headers({ 'Content-Type': 'application/json' });
        let options = new RequestOptions({ headers: cpHeaders });
        return this.http.put(this.managerUrl + "/" + manager.personId, manager, options)
            .map(success => success.status)
            .catch(this.handleError);
    }

    //Delete manager	
    deleteManagerById(managerId: string): Observable<number> {
        let cpHeaders = new Headers({ 'Content-Type': 'application/json' });
        let options = new RequestOptions({ headers: cpHeaders });
        return this.http.delete(this.managerUrl + "/" + managerId)
            .map(success => success.status)
            .catch(this.handleError);
    }

    private extractData(res: Response) {
        let body = res.json();
        return body;
    }
    
    private handleError(error: Response | any) {
        console.error(error.message || error);
        return Observable.throw(error.status);
    }
}