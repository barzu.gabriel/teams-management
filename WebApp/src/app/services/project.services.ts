import { Injectable } from '@angular/core';
import { Http, Response, Headers, URLSearchParams, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

import { Project } from '../entities/project.entities';

@Injectable()
export class ProjectService {
    //URL for CRUD operations
    projectUrl = "http://localhost:8080/TeamsManagement/rest/projects";

    //Create constructor to get Http instance
    constructor(private http: Http) {
    }

    //Fetch all projects
    getAllProjects(): Observable<Project[]> {
        return this.http.get(this.projectUrl)
            .map(this.extractData)
            .catch(this.handleError);
    }

    //Create project
    createProject(project: Project): Observable<number> {
        let cpHeaders = new Headers({ 'Content-Type': 'application/json' });
        let options = new RequestOptions({ headers: cpHeaders });
        return this.http.post(this.projectUrl, project, options)
            .map(success => success.status)
            .catch(this.handleError);
    }

    //Fetch project by id
    getProjectById(projectId: string): Observable<Project> {
        let cpHeaders = new Headers({ 'Content-Type': 'application/json' });
        let options = new RequestOptions({ headers: cpHeaders });
        console.log(this.projectUrl + "/" + projectId);
        return this.http.get(this.projectUrl + "/" + projectId)
            .map(this.extractData)
            .catch(this.handleError);
    }

    //Update project
    updateProject(project: Project): Observable<number> {
        let cpHeaders = new Headers({ 'Content-Type': 'application/json' });
        let options = new RequestOptions({ headers: cpHeaders });
        return this.http.put(this.projectUrl + "/" + project.projectId, project, options)
            .map(success => success.status)
            .catch(this.handleError);
    }

    //Delete project
    deleteProjectById(projectId: string): Observable<number> {
        let cpHeaders = new Headers({ 'Content-Type': 'application/json' });
        let options = new RequestOptions({ headers: cpHeaders });
        return this.http.delete(this.projectUrl + "/" + projectId)
            .map(success => success.status)
            .catch(this.handleError);
    }

    private extractData(res: Response) {
        let body = res.json();
        return body;
    }

    private handleError(error: Response | any) {
        console.error(error.message || error);
        return Observable.throw(error.status);
    }
}