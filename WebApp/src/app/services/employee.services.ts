import { Injectable } from '@angular/core';
import { Http, Response, Headers, URLSearchParams, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

import { Employee } from '../entities/employee.entities';

@Injectable()
export class EmployeeService {

    //URL for CRUD operations
    employeeUrl = "http://localhost:8080/TeamsManagement/rest/employees";

    //Create constructor to get Http instance
    constructor(private http: Http) {
    }
    
    //Fetch all employees
    getAllEmployees(): Observable<Employee[]> {
        return this.http.get(this.employeeUrl)
            .map(this.extractData)
            .catch(this.handleError);
    }

    //Create employee
    createEmployee(employee: Employee): Observable<number> {
        let cpHeaders = new Headers({ 'Content-Type': 'application/json' });
        let options = new RequestOptions({ headers: cpHeaders });
        return this.http.post(this.employeeUrl, employee, options)
            .map(success => success.status)
            .catch(this.handleError);
    }

    //Fetch employee by id
    getEmployeeById(employeeId: string): Observable<Employee> {
        let cpHeaders = new Headers({ 'Content-Type': 'application/json' });
        let options = new RequestOptions({ headers: cpHeaders });
        console.log(this.employeeUrl + "/" + employeeId);
        return this.http.get(this.employeeUrl + "/" + employeeId)
            .map(this.extractData)
            .catch(this.handleError);
    }

    //Update employee
    updateEmployee(employee: Employee): Observable<number> {
        let cpHeaders = new Headers({ 'Content-Type': 'application/json' });
        let options = new RequestOptions({ headers: cpHeaders });
        return this.http.put(this.employeeUrl + "/" + employee.personId, employee, options)
            .map(success => success.status)
            .catch(this.handleError);
    }

    //Delete employee	
    deleteEmployeeById(employeeId: string): Observable<number> {
        let cpHeaders = new Headers({ 'Content-Type': 'application/json' });
        let options = new RequestOptions({ headers: cpHeaders });
        return this.http.delete(this.employeeUrl + "/" + employeeId)
            .map(success => success.status)
            .catch(this.handleError);
    }

    private extractData(res: Response) {
        let body = res.json();
        return body;
    }
    
    private handleError(error: Response | any) {
        console.error(error.message || error);
        return Observable.throw(error.status);
    }
}