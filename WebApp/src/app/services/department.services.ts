import { Injectable } from '@angular/core';
import { Http, Response, Headers, URLSearchParams, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

import { Department } from '../entities/department.entities';

@Injectable()
export class DepartmentService {

    //URL for CRUD operations
    departmentUrl = "http://localhost:8080/TeamsManagement/rest/departments";

    //Create constructor to get Http instance
    constructor(private http: Http) {
    }

    //Fetch all departments
    getAllDepartments(): Observable<Department[]> {
        return this.http.get(this.departmentUrl)
            .map(this.extractData)
            .catch(this.handleError);
    }

    //Create department
    createDepartment(department: Department): Observable<number> {
        let cpHeaders = new Headers({ 'Content-Type': 'application/json' });
        let options = new RequestOptions({ headers: cpHeaders });
        return this.http.post(this.departmentUrl, department, options)
            .map(success => success.status)
            .catch(this.handleError);
    }

    //Fetch department by id
    getDepartmentById(departmentId: string): Observable<Department> {
        let cpHeaders = new Headers({ 'Content-Type': 'application/json' });
        let options = new RequestOptions({ headers: cpHeaders });
        console.log(this.departmentUrl + "/" + departmentId);
        return this.http.get(this.departmentUrl + "/" + departmentId)
            .map(this.extractData)
            .catch(this.handleError);
    }

    //Update department
    updateDepartment(department: Department): Observable<number> {
        let cpHeaders = new Headers({ 'Content-Type': 'application/json' });
        let options = new RequestOptions({ headers: cpHeaders });
        return this.http.put(this.departmentUrl + "/" + department.departmentId, department, options)
            .map(success => success.status)
            .catch(this.handleError);
    }

    //Delete department	
    deleteDepartmentById(departmentId: string): Observable<number> {
        let cpHeaders = new Headers({ 'Content-Type': 'application/json' });
        let options = new RequestOptions({ headers: cpHeaders });
        return this.http.delete(this.departmentUrl + "/" + departmentId)
            .map(success => success.status)
            .catch(this.handleError);
    }

    private extractData(res: Response) {
        let body = res.json();
        return body;
    }

    private handleError(error: Response | any) {
        console.error(error.message || error);
        return Observable.throw(error.status);
    }
}