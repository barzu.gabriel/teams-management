import { Component, OnInit } from '@angular/core';

import { Department } from '../entities/department.entities';
import { DepartmentService } from '../services/department.services';

import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Project } from '../entities/project.entities';

@Component({
    selector: 'app-department',
    templateUrl: './department.component.html',
    styleUrls: ['./department.component.css']
})
export class DepartmentComponent implements OnInit {
    departaments: Department[];
    statusCode: number;
    requestProcessing = false;
    processValidation = false;
    showFormValue = false;
    currentDepartmentId : number;

    departmentIdToUpdate = null;

    constructor(private departmentService: DepartmentService) {
    }

    ngOnInit(): void {
        this.getAllDepartments();
    }

    getAllDepartments() {
        this.departmentService.getAllDepartments()
            .subscribe(
                data => this.departaments = data,
                errorCode => this.statusCode = errorCode);
    }

    showForm(){
        this.showFormValue = this.showFormValue ? false : true;
    }

    //Load department by id to edit
    loadDepartmentToEdit(departmentId: string) {
        this.showFormValue = true;
        this.preProcessConfigurations();
        this.departmentService.getDepartmentById(departmentId)
            .subscribe(
                department => {
                    this.departmentIdToUpdate = department.departmentId;
                    this.departmentForm.setValue({ departmentId: department.departmentId, departmentName: department.departmentName });
                    this.requestProcessing = false;
                    this.processValidation = true;
                },
                errorCode => this.statusCode = errorCode
            );
    }

    //Delete department
    deleteDepartment(departmentId: string) {
        this.preProcessConfigurations();
        this.departmentService.deleteDepartmentById(departmentId)
            .subscribe(successCode => {
                this.statusCode = 204;
                this.getAllDepartments();
                this.backToCreateDepartment();
            },
                errorCode => this.statusCode = errorCode);
    }

    //Create form
    departmentForm = new FormGroup({
        departmentId: new FormControl('', Validators.required),
        departmentName: new FormControl('', Validators.required)
    });

    //Handle create and update department
    onDepartmentFormSubmit() {
        this.processValidation = true;
        if (this.departmentForm.invalid) {
            return; //Validation failed, exit from method.
        }
        //Form is valid, now perform create or update
        this.preProcessConfigurations();
        let department = this.departmentForm.value;
        if (this.departmentIdToUpdate === null) {
            //Generate department id then create department
            this.departmentService.getAllDepartments()
                .subscribe(departments => {
                    //Create department
                    this.departmentService.createDepartment(department)
                        .subscribe(successCode => {
                            this.statusCode = successCode;
                            this.getAllDepartments();
                            this.backToCreateDepartment();
                        },
                            errorCode => this.statusCode = errorCode
                        );
                });
        } else {
            //Handle update department
            department.departmentId = this.departmentIdToUpdate;
            this.departmentService.updateDepartment(department)
                .subscribe(successCode => {
                    this.statusCode = successCode;
                    this.getAllDepartments();
                    this.backToCreateDepartment();
                },
                    errorCode => this.statusCode = errorCode);
        }
    }

    //Perform preliminary processing configurations
    preProcessConfigurations() {
        this.statusCode = null;
        this.requestProcessing = true;
    }

    //Go back from update to create
    backToCreateDepartment() {
        this.departmentIdToUpdate = null;
        this.processValidation = false;
        this.showFormValue = false;
        this.departmentForm.reset();
    }
}
