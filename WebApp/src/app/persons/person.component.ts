import { Component, OnInit } from '@angular/core';

import { Employee} from '../entities/employee.entities';
import { Manager} from '../entities/manager.entities';
import { EmployeeService } from '../services/employee.services';
import { ManagerService } from '../services/manager.services';

@Component({
    selector: 'projectList',
    templateUrl: './person.component.html',
})
export class PersonComponent implements OnInit {
    employees: Employee[];
    managers: Manager[];
    statusCode: number;
    requestProcessing = false;

    personIdToUpdate = null;

    constructor(private employeeService: EmployeeService, private managerService: ManagerService) {
    }

    ngOnInit(): void {
        this.getAllEmployees();
        this.getAllManagers();
    }

    getAllEmployees() {
        this.employeeService.getAllEmployees()
            .subscribe(
                data => this.employees = data,
                errorCode => this.statusCode = errorCode);
    }

    getAllManagers() {
        this.managerService.getAllManagers()
            .subscribe(
                data => this.managers = data,
                errorCode => this.statusCode = errorCode);
    }

    //Load employee by id to edit
    loadEmployeeToEdit(employeeId: string) {
        this.preProcessConfigurations();
        this.employeeService.getEmployeeById(employeeId)
            .subscribe(
                employee => {
                    this.personIdToUpdate = employee.personId;
                    this.requestProcessing = false;
                },
                errorCode => this.statusCode = errorCode
            );
    }
    preProcessConfigurations() {
        this.statusCode = null;
        this.requestProcessing = true;
    }
}
