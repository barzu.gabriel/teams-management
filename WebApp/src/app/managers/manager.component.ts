import { Component, OnInit } from '@angular/core';

import { Manager } from '../entities/manager.entities';
import { ManagerService } from '../services/manager.services';

import { FormControl, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-manager',
  templateUrl: './manager.component.html',
  styleUrls: ['./manager.component.css']
})
export class ManagersComponent implements OnInit {

  managers: Manager[];
  statusCode: number;
  requestProcessing = false;
  processValidation = false;
  showFormValue = false;

  managerIdToUpdate = null;

  constructor(private managerService: ManagerService) {
  }

  ngOnInit() {
    this.getAllManagers();

  }

  showForm() {
    this.showFormValue = this.showFormValue ? false : true;
  }

  getAllManagers() {
    this.managerService.getAllManagers()
      .subscribe(
        data => this.managers = data,
        errorCode => this.statusCode = errorCode);
  }

  //Load manager by id to edit
  loadManagerToEdit(managerId: string) {
    this.showFormValue = true;
    this.preProcessConfigurations();
    this.managerService.getManagerById(managerId)
      .subscribe(
        manager => {
          this.managerIdToUpdate = manager.personId;
          this.managerForm.setValue({
            personId: manager.personId,
            firstName: manager.firstName,
            lastName: manager.lastName,
            email: manager.email,
            hiringDate: manager.hiringDate,
            contactPhone: manager.contactPhone,
            departmentId: manager.department.departmentId
          });
          this.requestProcessing = false;
          this.processValidation = true;
        },
        errorCode => this.statusCode = errorCode
      );
  }

  //Delete manager
  deleteManager(managerId: string) {
    this.preProcessConfigurations();
    this.managerService.deleteManagerById(managerId)
      .subscribe(successCode => {
        this.statusCode = 204;
        this.getAllManagers();
        this.backToCreateManager();
      },
        errorCode => this.statusCode = errorCode);
  }

  //Create form
  managerForm = new FormGroup({
    personId: new FormControl('', Validators.required),
    firstName: new FormControl('', Validators.required),
    lastName: new FormControl('', Validators.required),
    email: new FormControl('', Validators.required),
    hiringDate: new FormControl('', Validators.required),
    contactPhone: new FormControl('', Validators.required),
    departmnetId: new FormControl('', Validators.required)
  });

  //Handle create and update manager
  onManagerFormSubmit() {
    this.processValidation = true;
    if (this.managerForm.invalid) {
      return; //Validation failed, exit from method.
    }
    //Form is valid, now perform create or update
    this.preProcessConfigurations();
    let manager = this.managerForm.value;
    if (this.managerIdToUpdate === null) {
      //Generate manager id then create manager
      this.managerService.getAllManagers()
        .subscribe(managers => {
          // let prjId = manager.departmnetId;
          delete manager.departmnetId;
          // manager.project = {};
          // manager.project.departmnetId = prjId;
          //Create manager
          this.managerService.createManager(manager)
            .subscribe(successCode => {
              this.statusCode = successCode;
              this.getAllManagers();
              this.backToCreateManager();
            },
              errorCode => this.statusCode = errorCode
            );
        });
    } else {
      //Handle update manager
      manager.managerId = this.managerIdToUpdate;
      // let prjId = manager.departmnetId;
      delete manager.departmnetId;
      // manager.project = {};
      // manager.project.departmnetId = prjId;
      this.managerService.updateManager(manager)
        .subscribe(successCode => {
          this.statusCode = successCode;
          this.getAllManagers();
          this.backToCreateManager();
        },
          errorCode => this.statusCode = errorCode);
    }
  }

  //Perform preliminary processing configurations
  preProcessConfigurations() {
    this.statusCode = null;
    this.requestProcessing = true;
  }

  //Go back from update to create
  backToCreateManager() {
    this.managerIdToUpdate = null;
    this.processValidation = false;
    this.showFormValue = false;
    this.managerForm.reset();
  }
}
