import { Component, OnInit } from '@angular/core';

import { Project } from '../entities/project.entities';
import { ProjectService } from '../services/project.services';

import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Department } from '../entities/department.entities';

@Component({
    selector: 'app-projects',
    templateUrl: './project.component.html',
    styleUrls: ['./project.component.css']
})
export class ProjectComponent implements OnInit {
    projects: Project[];
    statusCode: number;
    requestProcessing = false;
    processValidation = false;
    showFormValue = false;

    projectIdToUpdate = null;

    constructor(private projectService: ProjectService) {
    }

    ngOnInit(): void {
        this.getAllProjects();
    }

    getAllProjects() {
        this.projectService.getAllProjects()
            .subscribe(
                data => this.projects = data,
                errorCode => this.statusCode = errorCode);
    }

    showForm() {
        this.showFormValue = this.showFormValue ? false : true;
    }

    //Load project by id to edit
    loadProjectToEdit(projectId: string) {
        this.showFormValue = true;
        this.preProcessConfigurations();
        this.projectService.getProjectById(projectId)
            .subscribe(
                project => {
                    this.projectIdToUpdate = project.projectId;
                    this.projectForm.setValue({ projectId: project.projectId, projectName: project.projectName, projectDescription: project.projectDescription, departmentId: project.department.departmentId });
                    this.requestProcessing = false;
                    this.processValidation = true;
                },
                errorCode => this.statusCode = errorCode
            );
    }


    //Delete project
    deleteProject(projectId: string) {
        this.preProcessConfigurations();
        this.projectService.deleteProjectById(projectId)
            .subscribe(successCode => {
                this.statusCode = 204;
                this.getAllProjects();
                this.backToCreateProject();
            },
                errorCode => this.statusCode = errorCode);
    }

    //Create form
    projectForm = new FormGroup({
        projectId: new FormControl('', Validators.required),
        projectName: new FormControl('', Validators.required),
        projectDescription: new FormControl('', Validators.required),
        departmentId: new FormControl('', Validators.required)
    });

    //Handle create and update project
    onProjectFormSubmit() {
        this.processValidation = true;
        if (this.projectForm.invalid) {
            return; //Validation failed, exit from method.
        }
        //Form is valid, now perform create or update
        this.preProcessConfigurations();
        let project = this.projectForm.value;
        if (this.projectIdToUpdate === null) {
            //Generate project id then create project
            this.projectService.getAllProjects()
                .subscribe(projects => {
                    let departId = project.departmentId;
                    delete project.departmentId;
                    project.department = {};
                    project.department.departmentId = departId;
                    //Create project
                    this.projectService.createProject(project)
                        .subscribe(successCode => {
                            this.statusCode = successCode;
                            this.getAllProjects();
                            this.backToCreateProject();
                        },
                            errorCode => this.statusCode = errorCode
                        );
                });
        } else {
            //Handle update project
            project.projectId = this.projectIdToUpdate;
            let departId = project.departmentId;
            delete project.departmentId;
            project.department = {};
            project.department.departmentId = departId;
            this.projectService.updateProject(project)
                .subscribe(successCode => {
                    this.statusCode = successCode;
                    this.getAllProjects();
                    this.backToCreateProject();
                },
                    errorCode => this.statusCode = errorCode);
        }
    }


    //Perform preliminary processing configurations
    preProcessConfigurations() {
        this.statusCode = null;
        this.requestProcessing = true;
    }

    //Go back from update to create
    backToCreateProject() {
        this.projectIdToUpdate = null;
        this.processValidation = false;
        this.showFormValue = false;
        this.projectForm.reset();
    }
}
