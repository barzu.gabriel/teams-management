import { Component, OnInit } from '@angular/core';

import { Employee } from '../entities/employee.entities';
import { EmployeeService } from '../services/employee.services';

import { FormControl, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-employee',
  templateUrl: './employee.component.html',
  styleUrls: ['./employee.component.css']
})
export class EmployeesComponent implements OnInit {

  employees: Employee[];
  statusCode: number;
  requestProcessing = false;
  processValidation = false;
  showFormValue = false;

  employeeIdToUpdate = null;

  constructor(private employeeService: EmployeeService) {
  }

  ngOnInit() {
    this.getAllEmployees();

  }

  showForm() {
    this.showFormValue = this.showFormValue ? false : true;
  }

  getAllEmployees() {
    this.employeeService.getAllEmployees()
      .subscribe(
        data => this.employees = data,
        errorCode => this.statusCode = errorCode);
  }

  //Load employee by id to edit
  loadEmployeeToEdit(employeeId: string) {
    this.showFormValue = true;
    this.preProcessConfigurations();
    this.employeeService.getEmployeeById(employeeId)
      .subscribe(
        employee => {
          this.employeeIdToUpdate = employee.personId;
          this.employeeForm.setValue({
            personId: employee.personId,
            firstName: employee.firstName,
            lastName: employee.lastName,
            email: employee.email,
            hiringDate: employee.hiringDate,
            projectId: employee.project.projectId
          });
          this.requestProcessing = false;
          this.processValidation = true;
        },
        errorCode => this.statusCode = errorCode
      );
  }

  //Delete employee
  deleteEmployee(employeeId: string) {
    this.preProcessConfigurations();
    this.employeeService.deleteEmployeeById(employeeId)
      .subscribe(successCode => {
        this.statusCode = 204;
        this.getAllEmployees();
        this.backToCreateEmployee();
      },
        errorCode => this.statusCode = errorCode);
  }

  //Create form
  employeeForm = new FormGroup({
    personId: new FormControl('', Validators.required),
    firstName: new FormControl('', Validators.required),
    lastName: new FormControl('', Validators.required),
    email: new FormControl('', Validators.required),
    hiringDate: new FormControl('', Validators.required),
    projectId: new FormControl('', Validators.required)
  });

  //Handle create and update employee
  onEmployeeFormSubmit() {
    this.processValidation = true;
    if (this.employeeForm.invalid) {
      return; //Validation failed, exit from method.
    }
    //Form is valid, now perform create or update
    this.preProcessConfigurations();
    let employee = this.employeeForm.value;
    if (this.employeeIdToUpdate === null) {
      //Generate employee id then create employee
      this.employeeService.getAllEmployees()
        .subscribe(employees => {
          let prjId = employee.projectId;
          delete employee.projectId;
          employee.project = {};
          employee.project.projectId = prjId;
          //Create employee
          this.employeeService.createEmployee(employee)
            .subscribe(successCode => {
              this.statusCode = successCode;
              this.getAllEmployees();
              this.backToCreateEmployee();
            },
              errorCode => this.statusCode = errorCode
            );
        });
    } else {
      //Handle update employee
      employee.employeeId = this.employeeIdToUpdate;
      let prjId = employee.projectId;
      delete employee.projectId;
      employee.project = {};
      employee.project.projectId = prjId;
      this.employeeService.updateEmployee(employee)
        .subscribe(successCode => {
          this.statusCode = successCode;
          this.getAllEmployees();
          this.backToCreateEmployee();
        },
          errorCode => this.statusCode = errorCode);
    }
  }

  //Perform preliminary processing configurations
  preProcessConfigurations() {
    this.statusCode = null;
    this.requestProcessing = true;
  }

  //Go back from update to create
  backToCreateEmployee() {
    this.employeeIdToUpdate = null;
    this.processValidation = false;
    this.showFormValue = false;
    this.employeeForm.reset();
  }
}
