import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpModule } from '@angular/http';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

import { RouterModule, Routes } from '@angular/router';

import { NavbarComponent } from './navbar/navbar.component';
import { ReactiveFormsModule } from '@angular/forms';

import { DepartmentComponent } from './departments/department.component';
import { DepartmentService } from './services/department.services';

import { ProjectComponent } from './projects/project.component';
import { ProjectService } from './services/project.services';

import { PersonComponent } from './persons/person.component';

import { EmployeesComponent } from './employees/employee.component';
import { EmployeeService } from './services/employee.services';

import { ManagersComponent } from './managers/manager.component';
import { ManagerService } from './services/manager.services';


@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    DepartmentComponent,
    ProjectComponent,
    PersonComponent,
    EmployeesComponent,
    ManagersComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpModule,
    ReactiveFormsModule,
    RouterModule.forRoot([
      { path: 'departments', component: DepartmentComponent },
      { path: 'projects', component: ProjectComponent },
      { path: 'employees', component: EmployeesComponent },
      { path: 'managers', component: ManagersComponent }
    ])
  ],
  providers: [
    DepartmentService,
    ProjectService,
    EmployeeService,
    ManagerService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
